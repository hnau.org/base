package org.hnau.base.utils.safe

import org.assertj.core.api.Assertions.assertThat
import org.hnau.base.test.TestUtils
import org.junit.Test
import kotlin.concurrent.thread


class SafeContextUnsafeTests {

    @Test
    fun test() {

        val safeContext = SafeContext.unsafe

        val result = ArrayList<Int>()

        listOf(
                thread {
                    safeContext.executeSafe {
                        result.add(0)
                        TestUtils.sleep(2)
                        result.add(2)
                    }
                },
                thread {
                    TestUtils.sleep(1)
                    safeContext.executeSafe {
                        result.add(1)
                    }
                }
        ).forEach { thread ->
            thread.join()
        }

        assertThat(result).isEqualTo(listOf(0, 1, 2))

    }

}