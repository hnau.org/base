package org.hnau.base.utils.safe.async

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.joinAll
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions.assertThat
import org.hnau.base.test.TestUtils
import org.junit.Test


class SafeContextAsyncMutexTests {

    @Test
    fun test() {

        val safeContext = SafeContextAsync.mutex()

        val result = ArrayList<Int>()

        runBlocking {
            listOf(
                    GlobalScope.launch {
                        safeContext.executeSafeAsync {
                            result.add(0)
                            TestUtils.sleep(2)
                            result.add(1)
                        }
                    },
                    GlobalScope.launch {
                        TestUtils.sleep(1)
                        safeContext.executeSafeAsync {
                            result.add(2)
                        }
                    }
            ).joinAll()
        }

        assertThat(result).isEqualTo(listOf(0, 1, 2))

    }

}