package org.hnau.base.utils.safe

import org.assertj.core.api.Assertions.assertThat
import org.hnau.base.test.TestUtils
import org.junit.Test
import kotlin.concurrent.thread


class SafeContextSynchronizedTests {

    @Test
    fun test() {

        val safeContext = SafeContext.synchronized()

        val result = ArrayList<Int>()

        listOf(
                thread {
                    safeContext.executeSafe {
                        result.add(0)
                        TestUtils.sleep(2)
                        result.add(1)
                    }
                },
                thread {
                    TestUtils.sleep(1)
                    safeContext.executeSafe {
                        result.add(2)
                    }
                }
        ).forEach { thread ->
            thread.join()
        }

        assertThat(result).isEqualTo(listOf(0, 1, 2))

    }

}