package org.hnau.base.utils.validator.charsequence.length

import org.assertj.core.api.Assertions
import org.hnau.base.utils.validator.Validator
import org.hnau.base.utils.validator.assertValidationError
import org.hnau.base.utils.validator.assertValidationSuccess
import org.hnau.base.utils.validator.size.MinSizeValidator
import org.hnau.base.utils.validator.size.minLength
import org.junit.Test


class StringMinLengthValidatorTests {

    @Test
    fun test() {

        val error = MinSizeValidator.Error(5)
        Assertions.assertThat(error.minLength).isEqualTo(5)

        val validator = Validator.minLength(5)

        validator.assertValidationError(
                value = "1234",
                error = MinSizeValidator.Error(minLength = 5)
        )

        validator.assertValidationSuccess(
                value = "123456"
        )

    }

}