package org.hnau.base.utils.validator.charsequence.length

import org.assertj.core.api.Assertions.assertThat
import org.hnau.base.utils.validator.Validator
import org.hnau.base.utils.validator.assertValidationError
import org.hnau.base.utils.validator.assertValidationSuccess
import org.hnau.base.utils.validator.size.MaxSizeValidator
import org.hnau.base.utils.validator.size.maxLength
import org.junit.Test


class StringMaxLengthValidatorTests {

    @Test
    fun test() {

        val error = MaxSizeValidator.Error(5)
        assertThat(error.maxLength).isEqualTo(5)

        val validator = Validator.maxLength(5)

        validator.assertValidationError(
                value = "123456",
                error = MaxSizeValidator.Error(maxLength = 5)
        )

        validator.assertValidationSuccess(
                value = "1234"
        )

    }

}