package org.hnau.base.utils.validator

import org.assertj.core.api.AbstractThrowableAssert
import org.hnau.base.test.assertThatThrownThrowable
import org.hnau.base.utils.validator.error.ValidatingError
import org.hnau.base.utils.validator.error.ValidatingException


fun <T> Validator<T>.assertValidationSuccess(
        value: T
) = validate(
        value = value
)

fun <T> Validator<T>.assertValidationError(
        value: T,
        error: ValidatingError
): AbstractThrowableAssert<*, *> = assertThatThrownThrowable {
    validate(
            value = value
    )
}.isEqualTo(
        ValidatingException(error)
)