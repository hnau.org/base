package org.hnau.base.utils.validator.charsequence.length

import org.hnau.base.utils.validator.Validator
import org.hnau.base.utils.validator.assertValidationError
import org.hnau.base.utils.validator.assertValidationSuccess
import org.hnau.base.utils.validator.size.MaxSizeValidator
import org.hnau.base.utils.validator.size.MinSizeValidator
import org.hnau.base.utils.validator.size.length
import org.junit.Test


class StringLengthValidatorTests {

    @Test
    fun test() {
        val validator = Validator.length(5..7)

        validator.assertValidationError(
                value = "1234",
                error = MinSizeValidator.Error(minLength = 5)
        )

        validator.assertValidationError(
                value = "12345678",
                error = MaxSizeValidator.Error(maxLength = 7)
        )

        validator.assertValidationSuccess(
                value = "123456"
        )

    }

}