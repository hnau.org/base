package org.hnau.base.utils.validator.charsequence.length

import org.hnau.base.utils.validator.Validator
import org.hnau.base.utils.validator.assertValidationError
import org.hnau.base.utils.validator.assertValidationSuccess
import org.hnau.base.utils.validator.size.MaxSizeValidator
import org.hnau.base.utils.validator.size.MinSizeValidator
import org.hnau.base.utils.validator.size.length
import org.junit.Test


class StringFixedLengthValidatorTests {

    @Test
    fun test() {
        val validator = Validator.length(6)

        validator.assertValidationError(
                value = "1234",
                error = MinSizeValidator.Error(minLength = 6)
        )

        validator.assertValidationError(
                value = "12345678",
                error = MaxSizeValidator.Error(maxLength = 6)
        )

        validator.assertValidationSuccess(
                value = "123456"
        )

    }

}