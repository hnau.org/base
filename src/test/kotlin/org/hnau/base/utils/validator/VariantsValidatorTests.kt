package org.hnau.base.utils.validator

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test


class VariantsValidatorTests {

    @Test
    fun test() {

        val variants = setOf(1, 2, 3)
        val validator = Validator.variants(variants)

        validator.assertValidationError(
                value = 4,
                error = VariantsValidator.Error()
        )

        validator.assertValidationSuccess(
                value = 2
        )

    }

}