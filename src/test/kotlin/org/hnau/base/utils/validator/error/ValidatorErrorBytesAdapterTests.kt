package org.hnau.base.utils.validator.error

import org.hnau.base.data.bytes.adapter.BytesAdapter
import org.hnau.base.data.bytes.adapter.byte
import org.hnau.base.data.bytes.adapter.toTypedAdapter
import org.hnau.base.data.bytes.adapter.typed
import org.hnau.base.data.locale.LocaleStructureValidatingError
import org.hnau.base.test.test
import org.hnau.base.utils.validator.EmailStructoreValidator
import org.hnau.base.utils.validator.VariantsValidator
import org.hnau.base.utils.validator.charsequence.CharsValidator
import org.hnau.base.utils.validator.size.MaxSizeValidator
import org.hnau.base.utils.validator.size.MinSizeValidator
import org.junit.Test


class ValidatorErrorBytesAdapterTests {

    @Test
    fun test() {

        lateinit var validatingErrorBytesAdapter: BytesAdapter<ValidatingError>
        validatingErrorBytesAdapter = BytesAdapter.typed(
                typedAdapters = mapOf(
                        0.toByte() to MaxSizeValidator.Error.bytesAdapter.toTypedAdapter(),
                        1.toByte() to MinSizeValidator.Error.bytesAdapter.toTypedAdapter(),
                        2.toByte() to EmailStructoreValidator.Error.bytesAdapter.toTypedAdapter(),
                        3.toByte() to CharsValidator.Error.bytesAdapter.toTypedAdapter(),
                        4.toByte() to LocaleStructureValidatingError.bytesAdapter.toTypedAdapter(),
                        5.toByte() to VariantsValidator.Error.bytesAdapter.toTypedAdapter()
                ),
                keyAdapter = BytesAdapter.byte

        )

        listOf(
                MaxSizeValidator.Error(90),
                MinSizeValidator.Error(10),
                EmailStructoreValidator.Error(),
                CharsValidator.Error(3, '^'),
                LocaleStructureValidatingError(),
                VariantsValidator.Error()
        ).forEach { error ->
            validatingErrorBytesAdapter.test(error)
        }

    }

}