package org.hnau.base.utils.validator.error

import org.assertj.core.api.Assertions.assertThat
import org.hnau.base.utils.validator.size.MaxSizeValidator
import org.junit.Test


class ValidatingExceptionTests {

    @Test
    fun test() {
        val error = MaxSizeValidator.Error(4)
        assertThat(ValidatingException(error).error).isEqualTo(error)
    }

}