package org.hnau.base.utils

import org.assertj.core.api.Assertions.assertThat
import org.hnau.base.data.maybe.Maybe
import org.hnau.base.data.maybe.error
import org.hnau.base.data.maybe.success
import org.hnau.base.extensions.doThrow
import org.hnau.base.test.WasCalled
import org.junit.Test


class TryUtilsTests {

    @Test
    fun tryOrElseTest() {

        fun tryExecuteAction(
                finally: WasCalled,
                onThrow: WasCalled,
                action: () -> String
        ): Maybe<String> = tryOrElse(
                action = {
                    val result = action()
                    Maybe.success(result)
                },
                onThrow = {
                    onThrow()
                    Maybe.error(it)
                },
                finally = finally::invoke
        )

        run {
            val exception = RuntimeException("Test")
            val finally = WasCalled("finally")
            val onThrow = WasCalled("onThrow")
            val actual = tryExecuteAction(
                    finally = finally,
                    onThrow = onThrow
            ) { throw exception }
            val expected = Maybe.error(exception)
            finally.assertWasCalled()
            onThrow.assertWasCalled()
            assertThat(actual).isEqualTo(expected)
        }

        run {
            val value = "Test"
            val finally = WasCalled("finally")
            val onThrow = WasCalled("onThrow")
            val actual = tryExecuteAction(
                    finally = finally,
                    onThrow = onThrow
            ) { value }
            val expected = Maybe.success(value)
            finally.assertWasCalled()
            onThrow.assertWasNotCalled()
            assertThat(actual).isEqualTo(expected)
        }

    }


    @Test
    fun tryOrFalseTest() {

        fun tryExecuteAction(
                finally: WasCalled,
                onThrow: WasCalled,
                action: () -> String
        ) = tryOrFalse(
                action = { action() },
                finally = finally::invoke,
                onThrow = { onThrow() }
        )

        run {
            val exception = RuntimeException("Test")
            val finally = WasCalled("finally")
            val onThrow = WasCalled("onThrow")
            val actual = tryExecuteAction(
                    finally = finally,
                    onThrow = onThrow
            ) { throw exception }
            val expected = false
            finally.assertWasCalled()
            onThrow.assertWasCalled()
            assertThat(actual).isEqualTo(expected)
        }

        run {
            val value = "Test"
            val finally = WasCalled("finally")
            val onThrow = WasCalled("onThrow")
            val actual = tryExecuteAction(
                    finally = finally,
                    onThrow = onThrow
            ) { value }
            val expected = true
            finally.assertWasCalled()
            onThrow.assertWasNotCalled()
            assertThat(actual).isEqualTo(expected)
        }

    }


    @Test
    fun tryOrNullTest() {

        fun tryExecuteAction(
                finally: WasCalled,
                onThrow: WasCalled,
                action: () -> String
        ) = tryOrNull(
                action = { action() },
                finally = finally::invoke,
                onThrow = { onThrow() }
        )

        run {
            val exception = RuntimeException("Test")
            val finally = WasCalled("finally")
            val onThrow = WasCalled("onThrow")
            val actual = tryExecuteAction(
                    finally = finally,
                    onThrow = onThrow
            ) { throw exception }
            finally.assertWasCalled()
            onThrow.assertWasCalled()
            assertThat(actual).isNull()
        }

        run {
            val value = "Test"
            val finally = WasCalled("finally")
            val onThrow = WasCalled("onThrow")
            val actual = tryExecuteAction(
                    finally = finally,
                    onThrow = onThrow
            ) { value }
            finally.assertWasCalled()
            onThrow.assertWasNotCalled()
            assertThat(actual).isEqualTo(value)
        }

    }

}