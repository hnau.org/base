package org.hnau.base.utils

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test


class EmptyTests {

    class EmptyImpl : Empty()

    @Test
    fun test() {

        val statelessObject = EmptyImpl()
        assertThat(statelessObject.toString()).isEqualTo("EmptyImpl()")
        assertThat(statelessObject.hashCode()).isEqualTo(EmptyImpl::class.java.hashCode())

    }

}