package org.hnau.base.utils


inline fun repeatTest(test: () -> Unit) =
        repeat(1000) { test() }