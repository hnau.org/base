package org.hnau.base

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test


class BitsAndBytesTests {

    @Test
    fun test() {

        listOf(
                0b1010101010101010 and 0xFF to 0b10101010,
                0b0101010101010101 and 0xFF to 0b01010101,
                0b1010101001010101 and 0xFF to 0b01010101,
                0b0101010110101010 and 0xFF to 0b10101010,
                0b1010101010101010 ushr 8 and 0xFF to 0b10101010,
                0b0101010101010101 ushr 8 and 0xFF to 0b01010101,
                0b1010101001010101 ushr 8 and 0xFF to 0b10101010,
                0b0101010110101010 ushr 8 and 0xFF to 0b01010101
        ).forEach { (actual, expected) ->
            assertThat(actual).isEqualTo(expected)
        }

    }

}