package org.hnau.base.extensions.uuid

import org.assertj.core.api.Assertions
import org.hnau.base.data.mapper.Mapper
import org.hnau.base.data.mapper.reversed
import org.hnau.base.test.test
import org.hnau.base.utils.repeatTest
import org.junit.Test
import java.util.*


class UUIDWithCompactStringExtensionsTest {

    @Test
    fun test() {

        repeatTest {
            Mapper.compactStringToUUID.reversed.test(UUID.randomUUID())
        }

        repeatTest {
            Assertions.assertThat(UUID.randomUUID().toCompactString().length)
                    .isEqualTo(UUIDCompactStringUtils.compactStringLength)
        }

    }

}