package org.hnau.base.extensions.uuid

import org.hnau.base.data.bytes.adapter.BytesAdapter
import org.hnau.base.data.mapper.Mapper
import org.hnau.base.test.test
import org.hnau.base.utils.repeatTest
import org.junit.Test
import java.util.*


class UUIDBytesAdapterExtensionsTests {

    @Test
    fun test() {

        repeatTest {
            BytesAdapter.uuid.test(UUID.randomUUID())
        }

    }

}