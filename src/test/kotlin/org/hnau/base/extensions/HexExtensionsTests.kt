package org.hnau.base.extensions

import org.junit.Test
import org.assertj.core.api.Assertions.assertThat
import org.hnau.base.data.mapper.Mapper
import org.hnau.base.data.mapper.stringToInt
import org.hnau.base.test.assertThatThrownThrowable
import org.hnau.base.test.test
import org.hnau.base.utils.extractThrownThrowable


class HexExtensionsTests {

    @Test
    fun test() {

        assertThat(extractThrownThrowable { "1".asHexToBytes() }.message)
                .isEqualTo("Hex string length must be even")

        assertThat(extractThrownThrowable { "0s".asHexToBytes() }.message)
                .isEqualTo("'s' is not correct Hex char")

        Mapper.hexStringToBytes.test("00ffFF7f80", String::toUpperCase)
    }

}