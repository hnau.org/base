package org.hnau.base.exception

import org.hnau.base.extensions.stackHead
import org.junit.Test
import kotlin.test.currentStackTrace
import org.assertj.core.api.Assertions.assertThat


class ThreadStackExtensionsTest {

    @Test
    fun test() {

        val stackHead = stackHead()
        currentStackTrace()

        assertThat(stackHead.className)
                .isEqualTo("org.hnau.base.exception.ThreadStackExtensionsTest")

        assertThat(stackHead.methodName)
                .isEqualTo("test")

    }

}