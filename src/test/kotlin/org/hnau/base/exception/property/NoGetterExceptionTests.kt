package org.hnau.base.exception.property

import org.hnau.base.test.assertThatThrownThrowable
import org.junit.Test


class NoGetterExceptionTests {

    @Test
    fun test() {
        assertThatThrownThrowable { NoGetterException.doThrow() }
                .isInstanceOf(NoGetterException::class.java)
    }

}