package org.hnau.base.data.settings

import org.assertj.core.api.Assertions.assertThat
import org.hnau.base.data.delegate.mutable.map
import org.hnau.base.data.mapper.Mapper
import org.hnau.base.extensions.boolean.ifFalse
import org.junit.Test


class SettingsTests {

    private class Settings(pathProvider: SettingsPathProvider<String, String>) : SettingsSection<String, String>(pathProvider) {

        class Auth(pathProvider: SettingsPathProvider<String, String>) : SettingsSection<String, String>(pathProvider) {

            var login by nextProvider("login")()
            var count by nextProvider("count")().map(Mapper(String::toInt, Int::toString))

        }

        val auth = Auth(nextProvider("auth"))

        var name by nextProvider("name")()

    }

    @Test
    fun test() {

        val cache = hashMapOf(
                "name" to "name",
                "auth.login" to "login",
                "auth.count" to "6"
        )

        fun createKey(path: Iterable<String>) = path.joinToString(".")
        fun throwKeyNotFoundException(key: String): Nothing = error("Value $key is not found")

        val connector = SettingsConnector.create<String, String>(
                read = { path ->
                    val key = createKey(path)
                    cache[key] ?: throwKeyNotFoundException(key)
                },
                write = { path, value ->
                    val key = createKey(path)
                    cache.containsKey(key).ifFalse { throwKeyNotFoundException(key) }
                    cache[key] = value
                }
        )

        val pathProvider = SettingsPathProvider(connector)

        val settings = Settings(pathProvider)

        assertThat(settings.name).isEqualTo("name")
        assertThat(settings.auth.login).isEqualTo("login")
        assertThat(settings.auth.count).isEqualTo(6)


        settings.auth.login = "newLogin"
        assertThat(settings.auth.login).isEqualTo("newLogin")

        settings.auth.count = 7
        assertThat(settings.auth.count).isEqualTo(7)

    }

}