package org.hnau.base.data.hashed

import org.junit.Test
import org.assertj.core.api.Assertions.assertThat


class HashedTests {

    @Test
    fun test() {

        val hashed = Hashed.fromString("test")

        assertThat(hashed.value.size)
                .isEqualTo(Hashed.validator.size.size)

        assertThat(hashed.let(Hashed.stringToHashedMapper.reverse).length)
                .isEqualTo(Hashed.encodedLength)

    }

}