package org.hnau.base.data.mapper

import org.hnau.base.extensions.plus
import org.junit.Test
import org.assertj.core.api.Assertions.assertThat


class MapperStringExtensionsTests {

    @Test
    fun testStringToStringsPairMapper() {

        val mappers = listOf(
                Mapper.stringToStringsPairByFirstLength,
                Mapper.stringToStringsPairBySeparator()
        ).map { mapper ->
            mapper.reverse + mapper.direct
        }

        listOf(
                "" to "",
                "" to "123",
                "123" to "",
                "123" to "123"
        ).forEach { pair ->
            mappers.forEach { mapper ->
                assertThat(mapper(pair)).isEqualTo(pair)
            }
        }

    }

}