package org.hnau.base.data.bytes

import org.hnau.base.data.bytes.adapter.BytesAdapter
import org.hnau.base.data.bytes.adapter.short
import org.hnau.base.data.bytes.adapter.map
import org.hnau.base.data.bytes.adapter.string
import org.hnau.base.data.bytes.provider.read
import org.hnau.base.data.bytes.provider.readArray
import org.hnau.base.data.bytes.provider.readBoolean
import org.hnau.base.data.bytes.provider.readBooleanArray
import org.hnau.base.data.bytes.provider.readByte
import org.hnau.base.data.bytes.provider.readByteArray
import org.hnau.base.data.bytes.provider.readChar
import org.hnau.base.data.bytes.provider.readDouble
import org.hnau.base.data.bytes.provider.readDoubleArray
import org.hnau.base.data.bytes.provider.readFloat
import org.hnau.base.data.bytes.provider.readFloatArray
import org.hnau.base.data.bytes.provider.readInt
import org.hnau.base.data.bytes.provider.readIntArray
import org.hnau.base.data.bytes.provider.readList
import org.hnau.base.data.bytes.provider.readLong
import org.hnau.base.data.bytes.provider.readLongArray
import org.hnau.base.data.bytes.provider.readShort
import org.hnau.base.data.bytes.provider.readShortArray
import org.hnau.base.data.bytes.provider.readString
import org.hnau.base.data.bytes.receiver.write
import org.hnau.base.data.bytes.receiver.writeArray
import org.hnau.base.data.bytes.receiver.writeBoolean
import org.hnau.base.data.bytes.receiver.writeBooleanArray
import org.hnau.base.data.bytes.receiver.writeByte
import org.hnau.base.data.bytes.receiver.writeByteArray
import org.hnau.base.data.bytes.receiver.writeChar
import org.hnau.base.data.bytes.receiver.writeCollection
import org.hnau.base.data.bytes.receiver.writeDouble
import org.hnau.base.data.bytes.receiver.writeDoubleArray
import org.hnau.base.data.bytes.receiver.writeFloat
import org.hnau.base.data.bytes.receiver.writeFloatArray
import org.hnau.base.data.bytes.receiver.writeInt
import org.hnau.base.data.bytes.receiver.writeIntArray
import org.hnau.base.data.bytes.receiver.writeLong
import org.hnau.base.data.bytes.receiver.writeLongArray
import org.hnau.base.data.bytes.receiver.writeShort
import org.hnau.base.data.bytes.receiver.writeShortArray
import org.hnau.base.data.bytes.receiver.writeString
import org.hnau.base.data.mapper.Mapper
import org.hnau.base.data.mapper.toEnum
import org.hnau.base.test.test
import org.junit.Test


class StreamExtensionsTests {

    @Test
    fun testDataClass() {

        DataClass.bytesAdapter.test(
                value = DataClass(
                        Primitive("\uF402\uF403\uF404"),
                        TestEnum.enum1,
                        '',
                        Byte.MIN_VALUE, (-47).toByte(), 0.toByte(), 47.toByte(), Byte.MAX_VALUE,
                        Short.MIN_VALUE, (-195).toShort(), 0.toShort(), 195.toShort(), Short.MAX_VALUE,
                        Int.MIN_VALUE, (-214748364).toInt(), 0.toInt(), 214748364.toInt(), Int.MAX_VALUE,
                        Long.MIN_VALUE, (-922337203685477580).toLong(), 0.toLong(), 922337203685477580.toLong(), Long.MAX_VALUE,
                        Float.MIN_VALUE, (-0.0062).toFloat(), 0.toFloat(), 0.0062.toFloat(), Float.MAX_VALUE, Float.NaN, Float.NEGATIVE_INFINITY, Float.POSITIVE_INFINITY,
                        Double.MIN_VALUE, (-0.0062).toDouble(), 0.toDouble(), 0.0062.toDouble(), Double.MAX_VALUE, Double.NaN, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY,
                        false, true,
                        "", "qwerty",
                        arrayOf(Primitive("\uF405\uF406"), Primitive("\uF63B\uF63C\uF63D")),
                        byteArrayOf(Byte.MIN_VALUE, Byte.MAX_VALUE),
                        shortArrayOf(Short.MIN_VALUE, Short.MAX_VALUE),
                        intArrayOf(Int.MIN_VALUE, Int.MAX_VALUE),
                        longArrayOf(Long.MIN_VALUE, Long.MAX_VALUE),
                        floatArrayOf(Float.MIN_VALUE, Float.MAX_VALUE),
                        doubleArrayOf(Double.MIN_VALUE, Double.MAX_VALUE),
                        booleanArrayOf(false, true),
                        listOf(Primitive(""), Primitive("")), listOf()
                ),
                toEqualCheckable = DataClass::toString
        )

    }

}

data class Primitive(
        val value: String
) {

    companion object {

        val stringToPrimitiveMapper = Mapper(::Primitive, Primitive::value)

        val bytesAdapter = BytesAdapter.string().map(stringToPrimitiveMapper)

    }

}

enum class TestEnum(
        val key: Short
) {

    enum0(
            key = 0
    ),

    enum1(
            key = 1
    ),

    enum2(
            key = 2
    );

    companion object {

        val shortToTestEnumMapper =
                Mapper.toEnum(TestEnum::key)

        val bytesAdapter =
                BytesAdapter.short.map(shortToTestEnumMapper)

    }

}

data class DataClass(
        val primitive: Primitive,
        val testEnum: TestEnum,
        val char: Char,
        val byte0: Byte, val byte1: Byte, val byte2: Byte, val byte3: Byte, val byte4: Byte,
        val short0: Short, val short1: Short, val short2: Short, val short3: Short, val short4: Short,
        val int0: Int, val int1: Int, val int2: Int, val int3: Int, val int4: Int,
        val long0: Long, val long1: Long, val long2: Long, val long3: Long, val long4: Long,
        val float0: Float, val float1: Float, val float2: Float, val float3: Float, val float4: Float, val float5: Float, val float6: Float, val float7: Float,
        val double0: Double, val double1: Double, val double2: Double, val double3: Double, val double4: Double, val double5: Double, val double6: Double, val double7: Double,
        val boolean0: Boolean, val boolean1: Boolean,
        val string0: String, val string1: String,
        val array: Array<Primitive>, val byteArray: ByteArray, val shortArray: ShortArray, val intArray: IntArray, val longArray: LongArray, val floatArray: FloatArray, val doubleArray: DoubleArray, val booleanArray: BooleanArray,
        val collection: Collection<Primitive>, val list: List<Primitive>
) {

    companion object {

        val bytesAdapter = BytesAdapter(
                read = {
                    DataClass(
                            read(Primitive.bytesAdapter.read),
                            read(TestEnum.bytesAdapter.read),
                            readChar(),
                            readByte(), readByte(), readByte(), readByte(), readByte(),
                            readShort(), readShort(), readShort(), readShort(), readShort(),
                            readInt(), readInt(), readInt(), readInt(), readInt(),
                            readLong(), readLong(), readLong(), readLong(), readLong(),
                            readFloat(), readFloat(), readFloat(), readFloat(), readFloat(), readFloat(), readFloat(), readFloat(),
                            readDouble(), readDouble(), readDouble(), readDouble(), readDouble(), readDouble(), readDouble(), readDouble(),
                            readBoolean(), readBoolean(),
                            readString(), readString(),
                            readArray(Primitive.bytesAdapter.read), readByteArray(), readShortArray(), readIntArray(), readLongArray(), readFloatArray(), readDoubleArray(), readBooleanArray(),
                            readList(Primitive.bytesAdapter.read),
                            readList(Primitive.bytesAdapter.read)
                    )
                },
                write = { dataClass ->
                    dataClass.run {
                        write(primitive, Primitive.bytesAdapter.write)
                        write(testEnum, TestEnum.bytesAdapter.write)
                        writeChar(char)
                        writeByte(byte0); writeByte(byte1); writeByte(byte2); writeByte(byte3); writeByte(byte4)
                        writeShort(short0); writeShort(short1); writeShort(short2); writeShort(short3); writeShort(short4)
                        writeInt(int0); writeInt(int1); writeInt(int2); writeInt(int3); writeInt(int4)
                        writeLong(long0); writeLong(long1); writeLong(long2); writeLong(long3); writeLong(long4)
                        writeFloat(float0); writeFloat(float1); writeFloat(float2); writeFloat(float3); writeFloat(float4); writeFloat(float5); writeFloat(float6); writeFloat(float7)
                        writeDouble(double0); writeDouble(double1); writeDouble(double2); writeDouble(double3); writeDouble(double4); writeDouble(double5); writeDouble(double6); writeDouble(double7)
                        writeBoolean(boolean0); writeBoolean(boolean1)
                        writeString(string0); writeString(string1)
                        writeArray(array, Primitive.bytesAdapter.write); writeByteArray(byteArray); writeShortArray(shortArray); writeIntArray(intArray); writeLongArray(longArray); writeFloatArray(floatArray); writeDoubleArray(doubleArray); writeBooleanArray(booleanArray)
                        writeCollection(collection, Primitive.bytesAdapter.write)
                        writeCollection(list, Primitive.bytesAdapter.write)
                    }
                }
        )

    }

}