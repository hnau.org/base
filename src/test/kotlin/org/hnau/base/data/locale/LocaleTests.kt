package org.hnau.base.data.locale

import org.assertj.core.api.Assertions.assertThat
import org.hnau.base.test.assertThatThrownThrowable
import org.hnau.base.test.test
import org.hnau.base.utils.validator.charsequence.CharsValidator
import org.hnau.base.utils.validator.error.ValidatingException
import org.hnau.base.utils.validator.error.exception
import org.hnau.base.utils.validator.size.MaxSizeValidator
import org.hnau.base.utils.validator.size.MinSizeValidator
import org.junit.Test


class LocaleTests {

    @Test
    fun testBytesAdapter() {

        listOf(
                Locale.Empty,
                Locale.Language(LocaleLanguage("ru")),
                Locale.LanguageWithRegion(LocaleLanguage("ru"), LocaleRegion("RU"))
        ).forEach { locale ->
            Locale.bytesAdapter.test(locale)
        }

    }

    @Test
    fun testMapper() {

        testWithSeparator(LocaleSeparator.default)

        testWithSeparator(LocaleSeparator("///"))

        assertThatThrownThrowable { LocaleSeparator("") }
                .isEqualTo(ValidatingException(MinSizeValidator.Error(1)))

        assertThatThrownThrowable { LocaleSeparator("-ru") }
                .isEqualTo(ValidatingException(CharsValidator.Error(1, 'r')))

    }

    private fun testWithSeparator(
            separator: LocaleSeparator
    ) {

        val mapper = Locale.stringToLocaleMapper(separator)
        val parser = Locale.parser(separator)

        fun testMapper(
                localeString: String
        ) = assertThat(
                localeString
                        .let(mapper.direct)
                        .let(mapper.reverse)
        ).isEqualTo(localeString)

        testMapper("")
        testMapper("ru")
        testMapper("ru${separator}RU")

        assertThatThrownThrowable { parser("ru${separator}RU${separator}RU") }
                .isEqualTo(LocaleStructureValidatingError().exception)

        assertThatThrownThrowable { parser("ru${separator}RU${separator}") }
                .isEqualTo(LocaleStructureValidatingError().exception)

        assertThatThrownThrowable { parser("${separator}RU${separator}RU") }
                .isEqualTo(LocaleStructureValidatingError().exception)

        assertThatThrownThrowable { parser("ru${separator}${separator}RU") }
                .isEqualTo(LocaleStructureValidatingError().exception)

        assertThatThrownThrowable { testMapper("ru${separator}ru") }
                .isEqualTo(CharsValidator.Error(0, 'r').exception)

        assertThatThrownThrowable { testMapper("RU${separator}RU") }
                .isEqualTo(CharsValidator.Error(0, 'R').exception)

        assertThatThrownThrowable { testMapper("r") }
                .isEqualTo(MinSizeValidator.Error(2).exception)

        assertThatThrownThrowable { testMapper("rua") }
                .isEqualTo(MaxSizeValidator.Error(2).exception)

        assertThatThrownThrowable { testMapper("ru${separator}") }
                .isEqualTo(MinSizeValidator.Error(2).exception)

        assertThatThrownThrowable { parser(separator.toString()) }
                .isEqualTo(MinSizeValidator.Error(2).exception)

        parser("ru${separator}RU")

    }

}