package org.hnau.base.temp

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.conflate
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.switchMap
import kotlinx.coroutines.joinAll
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.hnau.base.data.time.Time
import org.hnau.base.data.time.now
import org.hnau.base.data.time.toLevelsString
import org.hnau.base.extensions.boolean.checkBoolean
import org.junit.Test


@ExperimentalCoroutinesApi
class FlowsTest {

    @Test
    fun test() {

        val source = MutableStateFlow(0)

        val isActive = MutableStateFlow(false)

        val sourcePlaceholder = emptyFlow<Int>()

        val scopedSource = isActive.flatMapLatest { active ->
            active.checkBoolean<Flow<Int>>(
                    ifTrue = { source },
                    ifFalse = { sourcePlaceholder }
            )
        }

        GlobalScope.launch {
            scopedSource.collect { value ->
                println("On new value: $value")
            }
        }

        

    }

}