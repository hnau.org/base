package org.hnau.base.test

import org.assertj.core.api.Assertions.assertThat
import org.hnau.base.data.bytes.adapter.BytesAdapter
import org.hnau.base.data.bytes.provider.BufferedBytesProvider
import org.hnau.base.data.bytes.provider.read
import org.hnau.base.data.bytes.receiver.BufferedBytesReceiver
import org.hnau.base.data.bytes.receiver.write
import org.hnau.base.extensions.it


inline fun <T, C> BytesAdapter<T>.test(
        value: T,
        toEqualCheckable: T.() -> C
): ByteArray {
    val bytes = BufferedBytesReceiver()
            .apply { write(value, write) }
            .buffer

    val bytesProvider = BufferedBytesProvider(bytes)

    val decodedValue = bytesProvider.read(read)

    assertThat(bytesProvider.available)
            .isEqualTo(0)

    assertThat(value.toEqualCheckable())
            .isEqualTo(decodedValue.toEqualCheckable())

    return bytes

}

fun <T> BytesAdapter<T>.test(
        value: T
) = test<T, T>(
        value = value,
        toEqualCheckable = ::it
)