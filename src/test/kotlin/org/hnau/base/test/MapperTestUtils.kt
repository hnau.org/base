package org.hnau.base.test

import org.assertj.core.api.Assertions
import org.hnau.base.data.bytes.adapter.BytesAdapter
import org.hnau.base.data.mapper.Mapper
import org.hnau.base.extensions.it


fun <I, O, C> Mapper<I, O>.test(
        value: I,
        toEqualCheckable: I.() -> C
) {

    val encodedValue = direct(value)
    val decodedValue = reverse(encodedValue)

    Assertions.assertThat(value.toEqualCheckable())
            .isEqualTo(decodedValue.toEqualCheckable())

}

fun <I, O> Mapper<I, O>.test(
        value: I
) = test<I, O, I>(
        value = value,
        toEqualCheckable = ::it
)