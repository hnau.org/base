package org.hnau.base.test

import org.assertj.core.api.AbstractThrowableAssert
import org.assertj.core.api.Assertions.assertThat
import org.hnau.base.data.time.asMilliseconds
import org.hnau.base.data.time.sleep
import org.hnau.base.utils.extractThrownThrowable


object TestUtils {

    private val quantumSleepTime = 30.asMilliseconds

    fun sleep(times: Int = 1) {
        (quantumSleepTime * times).sleep()
    }

}

inline fun assertThatThrownThrowable(
        action: () -> Unit
): AbstractThrowableAssert<*, out Throwable> = assertThat(
        extractThrownThrowable(action = action)
)