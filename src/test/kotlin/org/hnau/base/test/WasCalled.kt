package org.hnau.base.test

import org.hnau.base.extensions.boolean.ifFalse
import org.hnau.base.extensions.boolean.ifTrue


class WasCalled(
        private val name: String
) : () -> Unit {

    private var wasCalled = false

    override operator fun invoke() {
        wasCalled = true
    }

    fun assertWasCalled() {
        wasCalled.ifFalse {
            error("$name was not called")
        }
    }

    fun assertWasNotCalled() {
        wasCalled.ifTrue {
            error("$name was called")
        }
    }

}