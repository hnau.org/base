package org.hnau.base.utils.base64

import org.hnau.base.data.bytes.adapter.BytesAdapter
import org.hnau.base.data.bytes.adapter.stateless
import org.hnau.base.extensions.boolean.ifFalse
import org.hnau.base.utils.Empty
import org.hnau.base.utils.tryOrFalse
import org.hnau.base.utils.validator.Validator
import org.hnau.base.utils.validator.charsequence.chars
import org.hnau.base.utils.validator.error.ValidatingError
import org.hnau.base.utils.validator.error.exception


fun Validator.Companion.base64Alphabet(
        alphabet: Base64Alphabet = Base64Alphabet.default
) = chars { char ->
    char in alphabet.chars
}
