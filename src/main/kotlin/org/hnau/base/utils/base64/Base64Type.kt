package org.hnau.base.utils.base64

import org.hnau.base.data.mapper.Mapper
import java.util.*


enum class Base64Type(
        val alphabet: Base64Alphabet,
        val decoder: Base64.Decoder,
        val encoder: Base64.Encoder
) {

    default(
            alphabet = Base64Alphabet.default,
            decoder = Base64.getDecoder(),
            encoder = Base64.getEncoder()
    ),

    urlSafe(
            alphabet = Base64Alphabet.urlSafe,
            decoder = Base64.getUrlDecoder(),
            encoder = Base64.getUrlEncoder()
    ),

    mime(
            alphabet = Base64Alphabet.default,
            decoder = Base64.getMimeDecoder(),
            encoder = Base64.getMimeEncoder()
    );

    val base64StringToBytesMapper
            by lazy { Mapper(::decode, ::encode) }

    fun encode(bytes: ByteArray): String =
            encoder.encodeToString(bytes)

    fun decode(string: String): ByteArray =
            decoder.decode(string)

}