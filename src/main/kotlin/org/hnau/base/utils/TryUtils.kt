package org.hnau.base.utils

import org.hnau.base.extensions.it


/**
 * Return result of [action], or result of [onThrow], if [action] had thrown Exception.
 * In the end execute [finally]
 */
inline fun <R> tryOrElse(
        action: () -> R,
        onThrow: (th: Throwable) -> R,
        finally: () -> Unit = {}
) = try {
    action()
} catch (th: Throwable) {
    onThrow(th)
} finally {
    finally()
}

/**
 * Return result of [action], or null after executing [onThrow], if [action] had thrown Exception.
 * In the end execute [finally]
 */
inline fun <R> tryOrNull(
        onThrow: (th: Throwable) -> Unit = {},
        finally: () -> Unit = {},
        action: () -> R
) = tryOrElse(
        action = action,
        onThrow = { onThrow(it); null },
        finally = finally
)
/**
 * Return `true` if execution of [action] was successful.
 * Return `false` after executing [onThrow], if [action] had thrown Exception.
 * In the end execute [finally]
 */
inline fun tryOrFalse(
        onThrow: (th: Throwable) -> Unit = {},
        finally: () -> Unit = {},
        action: () -> Unit
) = tryOrElse(
        action = { action(); true },
        onThrow = { onThrow(it); false },
        finally = finally
)