package org.hnau.base.utils

import org.hnau.base.extensions.it


inline fun extractThrownThrowable(
        onNotThrowed: () -> Throwable = { error("Throwable was not throwed") },
        action: () -> Unit
) = tryOrElse(
        action = {
            action()
            onNotThrowed()
        },
        onThrow = ::it
)

inline fun extractThrownThrowableOrNull(
        action: () -> Unit
) = tryOrElse(
        action = {
            action()
            null
        },
        onThrow = ::it
)