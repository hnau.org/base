package org.hnau.base.utils

import org.hnau.base.data.time.Time
import org.hnau.base.data.time.asMilliseconds
import java.util.*


object TimeZoneUtils {

    val UTC: TimeZone = TimeZone.getTimeZone("UTC")

}

val TimeZone.offset: Time
    get() = this.rawOffset.asMilliseconds
