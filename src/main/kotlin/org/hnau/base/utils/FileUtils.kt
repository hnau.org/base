package org.hnau.base.utils

import java.io.File


object FileUtils {

    fun createFileFromResources(
            path: String
    ) = javaClass.classLoader
            .getResource(path)
            ?.path
            ?.let(::File)

}