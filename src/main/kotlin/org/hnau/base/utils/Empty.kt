package org.hnau.base.utils


abstract class Empty {

    override fun toString() = "${javaClass.simpleName}()"

    override fun equals(other: Any?) =
            javaClass == other?.javaClass

    override fun hashCode() = javaClass.hashCode()

}