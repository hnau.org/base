package org.hnau.base.utils.validator

import org.hnau.base.data.bytes.adapter.BytesAdapter
import org.hnau.base.data.bytes.adapter.stateless
import org.hnau.base.extensions.boolean.ifFalse
import org.hnau.base.utils.Empty
import org.hnau.base.utils.validator.error.ValidatingError
import org.hnau.base.utils.validator.error.exception


class VariantsValidator<T>(
        val variants: Set<T>
) : Validator<T> {

    class Error : Empty(), ValidatingError {

        companion object {

            val bytesAdapter =
                    BytesAdapter.stateless(::Error)

        }

    }

    override fun validate(value: T) {
        (value in variants).ifFalse { throw  Error().exception }
    }

}


fun <T> Validator.Companion.variants(
        variants: Set<T>
) = VariantsValidator(
        variants = variants
)