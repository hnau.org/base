package org.hnau.base.utils.validator

import org.hnau.base.data.bytes.adapter.BytesAdapter
import org.hnau.base.data.bytes.adapter.stateless
import org.hnau.base.extensions.boolean.ifFalse
import org.hnau.base.utils.Empty
import org.hnau.base.utils.validator.error.ValidatingError
import org.hnau.base.utils.validator.error.exception


object EmailStructoreValidator : Validator<CharSequence> {

    class Error : Empty(), ValidatingError {

        companion object {

            val bytesAdapter =
                    BytesAdapter.stateless(::Error)

        }

    }

    override fun validate(value: CharSequence) {
        val parts = value.split("@")
        (parts.size == 2).ifFalse { throw Error().exception }
        parts.forEach { part -> part.isNotEmpty().ifFalse { throw Error().exception } }
    }

}

val Validator.Companion.emailStructure
    get() = EmailStructoreValidator