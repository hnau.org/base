package org.hnau.base.utils.validator.error


data class ValidatingException(
        val error: ValidatingError
) : IllegalArgumentException()