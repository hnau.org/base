package org.hnau.base.utils.validator


open class PairValidator<in T, A : Validator<T>, B : Validator<T>>(
        val first: A,
        val second: B
) : Validator<T> {

    override fun validate(
            value: T
    ) {
        first.validate(value)
        second.validate(value)
    }

}

fun <T, A : Validator<T>, B : Validator<T>> Validator.Companion.pair(
        first: A,
        second: B
) = PairValidator(
        first = first,
        second = second
)

infix fun <T, A : Validator<T>, B : Validator<T>> A.and(other: B) =
        Validator.pair(this, other)