package org.hnau.base.utils.validator.size

import org.hnau.base.data.bytes.adapter.BytesAdapter
import org.hnau.base.data.bytes.adapter.int
import org.hnau.base.data.bytes.adapter.map
import org.hnau.base.data.mapper.Mapper
import org.hnau.base.extensions.doThrow
import org.hnau.base.extensions.ifLargeThan
import org.hnau.base.extensions.ifLessThan
import org.hnau.base.utils.validator.Validator
import org.hnau.base.utils.validator.error.ValidatingError
import org.hnau.base.utils.validator.error.exception


class MaxSizeValidator<T>(
        val maxSize: Int,
        private val getSize: T.() -> Int
) : Validator<T> {

    data class Error(
            val maxLength: Int
    ) : ValidatingError {

        companion object {

            val bytesAdapter =
                    BytesAdapter.int.map(Mapper(::Error, Error::maxLength))

        }

    }

    override fun validate(value: T) {
        value.getSize().ifLargeThan(maxSize) { throw Error(maxSize).exception }
    }

}


fun <T> Validator.Companion.maxSize(
        maxSize: Int,
        getSize: T.() -> Int
) = MaxSizeValidator(
        maxSize = maxSize,
        getSize = getSize
)

fun Validator.Companion.maxSize(
        maxSize: Int
) = maxSize<Collection<*>>(
        maxSize = maxSize,
        getSize = { this.size }
)

fun Validator.Companion.maxArraySize(
        maxSize: Int
) = maxSize<Array<*>>(
        maxSize = maxSize,
        getSize = { this.size }
)

fun Validator.Companion.maxByteArraySize(
        maxSize: Int
) = maxSize<ByteArray>(
        maxSize = maxSize,
        getSize = { this.size }
)

fun Validator.Companion.maxShortArraySize(
        maxSize: Int
) = maxSize<ShortArray>(
        maxSize = maxSize,
        getSize = { this.size }
)

fun Validator.Companion.maxIntArraySize(
        maxSize: Int
) = maxSize<IntArray>(
        maxSize = maxSize,
        getSize = { this.size }
)

fun Validator.Companion.maxLongArraySize(
        maxSize: Int
) = maxSize<LongArray>(
        maxSize = maxSize,
        getSize = { this.size }
)

fun Validator.Companion.maxFloatArraySize(
        maxSize: Int
) = maxSize<FloatArray>(
        maxSize = maxSize,
        getSize = { this.size }
)

fun Validator.Companion.maxDoubleArraySize(
        maxSize: Int
) = maxSize<DoubleArray>(
        maxSize = maxSize,
        getSize = { this.size }
)

fun Validator.Companion.maxBooleanArraySize(
        maxSize: Int
) = maxSize<BooleanArray>(
        maxSize = maxSize,
        getSize = { this.size }
)

fun Validator.Companion.maxLength(
        maxLength: Int
) = maxSize<CharSequence>(
        maxSize = maxLength,
        getSize = { this.length }
)