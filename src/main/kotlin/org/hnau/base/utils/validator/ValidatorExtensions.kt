package org.hnau.base.utils.validator

import org.hnau.base.extensions.cast
import org.hnau.base.utils.extractThrownThrowableOrNull
import org.hnau.base.utils.validator.error.ValidatingException


fun <T> Validator<T>.validateOrError(value: T) =
        extractThrownThrowableOrNull { validate(value) }
                ?.cast<ValidatingException>()
                ?.error