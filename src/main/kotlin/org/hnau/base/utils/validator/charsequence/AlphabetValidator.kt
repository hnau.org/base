package org.hnau.base.utils.validator.charsequence

import org.hnau.base.utils.validator.Validator


class AlphabetValidator(
        val alphabet: Iterable<Char>
) : CharsValidator(
        checkChar = alphabet::contains
)


fun Validator.Companion.alphabet(
        alphabet: Iterable<Char>
) = AlphabetValidator(
        alphabet = alphabet
)