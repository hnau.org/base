package org.hnau.base.utils.validator.charsequence

import org.hnau.base.data.bytes.adapter.BytesAdapter
import org.hnau.base.data.bytes.provider.readChar
import org.hnau.base.data.bytes.provider.readInt
import org.hnau.base.data.bytes.receiver.writeChar
import org.hnau.base.data.bytes.receiver.writeInt
import org.hnau.base.extensions.boolean.ifFalse
import org.hnau.base.utils.validator.Validator
import org.hnau.base.utils.validator.error.PositionValidatingError
import org.hnau.base.utils.validator.error.exception


open class CharsValidator(
        val checkChar: (Char) -> Boolean
) : Validator<CharSequence> {

    data class Error(
            override val position: Int,
            val char: Char
    ) : PositionValidatingError {

        companion object {

            val bytesAdapter = BytesAdapter(
                    read = {
                        Error(
                                position = readInt(),
                                char = readChar()
                        )
                    },
                    write = { error ->
                        writeInt(error.position)
                        writeChar(error.char)
                    }
            )

        }

    }

    override fun validate(value: CharSequence) {
        value.forEachIndexed { position, char ->
            checkChar(char).ifFalse {
                throw Error(
                        position = position,
                        char = char
                ).exception
            }
        }
    }

}


fun Validator.Companion.chars(
        checkChar: (Char) -> Boolean
) = CharsValidator(
        checkChar = checkChar
)