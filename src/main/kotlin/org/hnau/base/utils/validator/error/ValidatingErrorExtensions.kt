package org.hnau.base.utils.validator.error


val ValidatingError.exception
    get() = ValidatingException(this)