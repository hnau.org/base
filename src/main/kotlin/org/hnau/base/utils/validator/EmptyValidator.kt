package org.hnau.base.utils.validator


object EmptyValidator : Validator<Any?> {

    override fun validate(value: Any?) {}

}

val Validator.Companion.empty
    get() = EmptyValidator