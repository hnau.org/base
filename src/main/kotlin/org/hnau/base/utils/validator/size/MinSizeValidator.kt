package org.hnau.base.utils.validator.size

import org.hnau.base.data.bytes.adapter.BytesAdapter
import org.hnau.base.data.bytes.adapter.int
import org.hnau.base.data.bytes.adapter.map
import org.hnau.base.data.mapper.Mapper
import org.hnau.base.extensions.doThrow
import org.hnau.base.extensions.ifLessThan
import org.hnau.base.utils.validator.Validator
import org.hnau.base.utils.validator.error.ValidatingError
import org.hnau.base.utils.validator.error.exception


class MinSizeValidator<T>(
        val minSize: Int,
        private val getSize: T.() -> Int
) : Validator<T> {

    data class Error(
            val minLength: Int
    ) : ValidatingError {

        companion object {

            val bytesAdapter =
                    BytesAdapter.int.map(Mapper(::Error, Error::minLength))

        }

    }

    override fun validate(value: T) {
        value.getSize().ifLessThan(minSize) { throw Error(minSize).exception }
    }

}


fun <T> Validator.Companion.minSize(
        minSize: Int,
        getSize: T.() -> Int
) = MinSizeValidator(
        minSize = minSize,
        getSize = getSize
)

fun Validator.Companion.minSize(
        minSize: Int
) = minSize<Collection<*>>(
        minSize = minSize,
        getSize = { this.size }
)

fun Validator.Companion.minArraySize(
        minSize: Int
) = minSize<Array<*>>(
        minSize = minSize,
        getSize = { this.size }
)

fun Validator.Companion.minByteArraySize(
        minSize: Int
) = minSize<ByteArray>(
        minSize = minSize,
        getSize = { this.size }
)

fun Validator.Companion.minShortArraySize(
        minSize: Int
) = minSize<ShortArray>(
        minSize = minSize,
        getSize = { this.size }
)

fun Validator.Companion.minIntArraySize(
        minSize: Int
) = minSize<IntArray>(
        minSize = minSize,
        getSize = { this.size }
)

fun Validator.Companion.minLongArraySize(
        minSize: Int
) = minSize<LongArray>(
        minSize = minSize,
        getSize = { this.size }
)

fun Validator.Companion.minFloatArraySize(
        minSize: Int
) = minSize<FloatArray>(
        minSize = minSize,
        getSize = { this.size }
)

fun Validator.Companion.minDoubleArraySize(
        minSize: Int
) = minSize<DoubleArray>(
        minSize = minSize,
        getSize = { this.size }
)

fun Validator.Companion.minBooleanArraySize(
        minSize: Int
) = minSize<BooleanArray>(
        minSize = minSize,
        getSize = { this.size }
)

fun Validator.Companion.minLength(
        minLength: Int
) = minSize<CharSequence>(
        minSize = minLength,
        getSize = { this.length }
)