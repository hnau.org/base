package org.hnau.base.utils.validator.error


interface PositionValidatingError: ValidatingError {

    val position: Int

}