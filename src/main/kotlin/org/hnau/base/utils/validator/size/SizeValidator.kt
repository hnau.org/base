package org.hnau.base.utils.validator.size

import org.hnau.base.utils.validator.PairValidator
import org.hnau.base.utils.validator.Validator
import org.hnau.base.utils.validator.and


sealed class Size {

    abstract val min: Int
    abstract val max: Int

    class Range(
            val size: ClosedRange<Int>
    ) : Size() {

        override val min: Int
            get() = size.start

        override val max: Int
            get() = size.endInclusive

    }

    class Fixed(
            val size: Int
    ) : Size() {

        override val min: Int
            get() = size

        override val max: Int
            get() = size

    }

}

sealed class SizeValidator<T, S : Size>(
        val size: S,
        getSize: T.() -> Int
) : PairValidator<T, MinSizeValidator<T>, MaxSizeValidator<T>>(
        Validator.minSize(size.min, getSize),
        Validator.maxSize(size.max, getSize)
) {

    class Range<T>(
            size: ClosedRange<Int>,
            getSize: T.() -> Int
    ) : SizeValidator<T, Size.Range>(
            size = Size.Range(size),
            getSize = getSize
    )

    class Fixed<T>(
            size: Int,
            getSize: T.() -> Int
    ) : SizeValidator<T, Size.Fixed>(
            size = Size.Fixed(size),
            getSize = getSize
    )

}


fun <T> Validator.Companion.size(
        size: ClosedRange<Int>,
        getSize: T.() -> Int
) = SizeValidator.Range(
        size = size,
        getSize = getSize
)

fun <T> Validator.Companion.size(
        size: Int,
        getSize: T.() -> Int
) = SizeValidator.Fixed(
        size = size,
        getSize = getSize
)

fun Validator.Companion.size(
        size: ClosedRange<Int>
) = size<Collection<*>>(
        size = size,
        getSize = { this.size }
)

fun Validator.Companion.size(
        size: Int
) = size<Collection<*>>(
        size = size,
        getSize = { this.size }
)

fun Validator.Companion.arraySize(
        size: ClosedRange<Int>
) = size<Array<*>>(
        size = size,
        getSize = { this.size }
)

fun Validator.Companion.arraySize(
        size: Int
) = size<Array<*>>(
        size = size,
        getSize = { this.size }
)

fun Validator.Companion.byteArraySize(
        size: ClosedRange<Int>
) = size<ByteArray>(
        size = size,
        getSize = { this.size }
)

fun Validator.Companion.byteArraySize(
        size: Int
) = size<ByteArray>(
        size = size,
        getSize = { this.size }
)

fun Validator.Companion.shortArraySize(
        size: ClosedRange<Int>
) = size<ShortArray>(
        size = size,
        getSize = { this.size }
)

fun Validator.Companion.shortArraySize(
        size: Int
) = size<ShortArray>(
        size = size,
        getSize = { this.size }
)

fun Validator.Companion.intArraySize(
        size: ClosedRange<Int>
) = size<IntArray>(
        size = size,
        getSize = { this.size }
)

fun Validator.Companion.intArraySize(
        size: Int
) = size<IntArray>(
        size = size,
        getSize = { this.size }
)

fun Validator.Companion.longArraySize(
        size: ClosedRange<Int>
) = size<LongArray>(
        size = size,
        getSize = { this.size }
)

fun Validator.Companion.longArraySize(
        size: Int
) = size<LongArray>(
        size = size,
        getSize = { this.size }
)

fun Validator.Companion.floatArraySize(
        size: ClosedRange<Int>
) = size<FloatArray>(
        size = size,
        getSize = { this.size }
)

fun Validator.Companion.floatArraySize(
        size: Int
) = size<FloatArray>(
        size = size,
        getSize = { this.size }
)

fun Validator.Companion.doubleArraySize(
        size: ClosedRange<Int>
) = size<DoubleArray>(
        size = size,
        getSize = { this.size }
)

fun Validator.Companion.doubleArraySize(
        size: Int
) = size<DoubleArray>(
        size = size,
        getSize = { this.size }
)

fun Validator.Companion.booleanArraySize(
        size: ClosedRange<Int>
) = size<BooleanArray>(
        size = size,
        getSize = { this.size }
)

fun Validator.Companion.booleanArraySize(
        size: Int
) = size<BooleanArray>(
        size = size,
        getSize = { this.size }
)

fun Validator.Companion.length(
        length: ClosedRange<Int>
) = size<CharSequence>(
        size = length,
        getSize = { this.length }
)

fun Validator.Companion.length(
        length: Int
) = size<CharSequence>(
        size = length,
        getSize = { this.length }
)