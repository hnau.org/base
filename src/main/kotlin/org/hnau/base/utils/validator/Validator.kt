package org.hnau.base.utils.validator

import org.hnau.base.utils.validator.error.ValidatingException


interface Validator<in T> {

    companion object;

    @Throws(ValidatingException::class)
    fun validate(value: T)

}