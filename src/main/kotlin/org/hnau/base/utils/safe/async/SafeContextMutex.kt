package org.hnau.base.utils.safe.async

import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import org.hnau.base.utils.safe.SafeContextSynchronized


class SafeContextSuspend : SafeContextSynchronized(), SafeContextAsync {

    private val mutex = Mutex()

    override suspend fun <R> executeSafeAsync(operation: suspend () -> R) =
            mutex.withLock { operation() }

}

fun SafeContextAsync.Companion.mutex() = SafeContextSuspend()