package org.hnau.base.utils.safe.async

import org.hnau.base.utils.safe.SafeContext


interface SafeContextAsync: SafeContext {

    companion object;

    suspend fun <R> executeSafeAsync(
            operation: suspend () -> R
    ): R

}