package org.hnau.base.utils.safe


open class SafeContextUnsafe protected constructor() : SafeContext {

    companion object {

        val instance = SafeContextUnsafe()

    }

    final override fun <R> executeSafe(operation: () -> R) = operation()

}

val SafeContext.Companion.unsafe get() = SafeContextUnsafe.instance