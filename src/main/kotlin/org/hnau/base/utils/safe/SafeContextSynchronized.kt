package org.hnau.base.utils.safe


open class SafeContextSynchronized : SafeContext {

    @Synchronized
    final override fun <R> executeSafe(operation: () -> R) = operation()

}

fun SafeContext.Companion.synchronized() = SafeContextSynchronized()