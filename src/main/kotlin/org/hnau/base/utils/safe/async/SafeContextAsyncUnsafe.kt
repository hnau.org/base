package org.hnau.base.utils.safe.async

import org.hnau.base.utils.safe.SafeContextUnsafe


object SafeContextAsyncUnsafe : SafeContextUnsafe(), SafeContextAsync {

    override suspend fun <R> executeSafeAsync(operation: suspend () -> R) = operation()

}

val SafeContextAsync.Companion.unsafe get() = SafeContextAsyncUnsafe