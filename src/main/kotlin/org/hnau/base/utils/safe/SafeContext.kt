package org.hnau.base.utils.safe


interface SafeContext {

    companion object

    fun <R> executeSafe(operation: () -> R): R

}