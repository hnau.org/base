package org.hnau.base.exception.property

import org.hnau.base.extensions.doThrow

class NoSetterException : Exception(message) {

    companion object {

        const val message = "Property does not have a setter"

        fun doThrow(): Nothing = NoSetterException().doThrow()

        @Suppress("UNUSED_PARAMETER")
        fun <T> doThrow(value: T): Nothing = doThrow()

    }

}