package org.hnau.base.exception.property

import org.hnau.base.extensions.doThrow

class NoGetterException : Exception(message) {

    companion object {

        const val message = "Property does not have a getter"

        fun doThrow(): Nothing = NoGetterException().doThrow()

        @Suppress("UNUSED_PARAMETER")
        fun <T> doThrow(value: T): Nothing = doThrow()

    }

}