package org.hnau.base.exception


object ShouldNeverHappenException: AssertionError("This should never happen")