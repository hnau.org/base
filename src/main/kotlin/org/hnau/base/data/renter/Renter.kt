package org.hnau.base.data.renter


interface Renter<out T> {

    companion object

    fun <R> rent(block: (T) -> R): R

    operator fun <R> invoke(p1: (T) -> R) = rent(p1)

}