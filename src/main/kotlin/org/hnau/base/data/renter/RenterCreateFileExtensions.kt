package org.hnau.base.data.renter

import java.io.File


val File.inputStreamRenter
    get() = Renter.createForClosable(::inputStream)

val File.outputStreamRenter
    get() = Renter.createForClosable(::outputStream)