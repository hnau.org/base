package org.hnau.base.data.renter


inline fun <T> Renter.Companion.create(
        crossinline create: () -> T,
        crossinline afterRent: (T) -> Unit
) = object : Renter<T> {

    override fun <R> rent(block: T.() -> R): R {
        val value = create()
        val result = block(value)
        afterRent(value)
        return result
    }

}

inline fun <T : AutoCloseable> Renter.Companion.createForClosable(
        crossinline create: () -> T
) = create(
        create = create,
        afterRent = { valueAfterUse -> valueAfterUse.close() }
)