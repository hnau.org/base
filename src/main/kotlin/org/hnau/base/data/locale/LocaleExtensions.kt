package org.hnau.base.data.locale

import org.hnau.base.extensions.it
import java.io.InputStream
import java.io.OutputStream


fun Locale.toString(
        separator: LocaleSeparator
) = when (this) {
    is Locale.LanguageWithRegion -> listOf(language, region)
    is Locale.Language -> listOf(language)
    Locale.Empty -> emptyList()
}.joinToString(
        separator = separator.value
)

fun Locale.Companion.maxLength(
        separator: LocaleSeparator = LocaleSeparator.default
) =
        LocaleRegion.length + separator.length + LocaleLanguage.length