package org.hnau.base.data.locale

import org.hnau.base.data.constraint.ReducableConstraint
import org.hnau.base.data.optional.Optional
import org.hnau.base.data.single.Single


sealed class Locale : ReducableConstraint<Locale> {

    companion object;

    object Empty : Locale() {

        override fun tryReduce(): Optional<Locale> = Optional.None

        override fun toString() =
                toString(LocaleSeparator.default)

    }

    data class Language(
            val language: LocaleLanguage
    ) : Locale() {

        override fun tryReduce(): Optional<Locale> = Optional.Some(Empty)

        override fun toString() =
                toString(LocaleSeparator.default)

    }

    data class LanguageWithRegion(
            val language: LocaleLanguage,
            val region: LocaleRegion
    ) : Locale() {

        override fun tryReduce(): Optional<Locale> = Optional.Some(Language(language))

        override fun toString() =
                toString(LocaleSeparator.default)

    }

}