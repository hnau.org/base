package org.hnau.base.data.locale

import org.hnau.base.utils.validator.Validator
import org.hnau.base.utils.validator.and
import org.hnau.base.utils.validator.charsequence.chars
import org.hnau.base.utils.validator.size.minLength


data class LocaleSeparator(
        val value: String
) {

    companion object {

        const val minLength = 1

        private val validator =
                Validator.minLength(minLength) and Validator.chars { !it.isLetter() }

        val default = LocaleSeparator("-")

    }

    init {
        validator.validate(value)
    }

    val length
        get() = value.length

    override fun toString() = value

}