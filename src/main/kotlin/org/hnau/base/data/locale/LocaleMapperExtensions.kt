package org.hnau.base.data.locale

import org.hnau.base.data.mapper.Mapper


fun Locale.Companion.stringToLocaleMapper(
        separator: LocaleSeparator = LocaleSeparator.default
) = Mapper(
        direct = parser(separator),
        reverse = { locale -> locale.toString(separator) }
)