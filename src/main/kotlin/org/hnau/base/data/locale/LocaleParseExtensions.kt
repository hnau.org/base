package org.hnau.base.data.locale

import org.hnau.base.data.bytes.adapter.BytesAdapter
import org.hnau.base.data.bytes.adapter.stateless
import org.hnau.base.extensions.boolean.ifFalse
import org.hnau.base.extensions.boolean.ifTrue
import org.hnau.base.extensions.elvis
import org.hnau.base.utils.Empty
import org.hnau.base.utils.validator.error.ValidatingError
import org.hnau.base.utils.validator.error.exception


class LocaleStructureValidatingError : Empty(), ValidatingError {

    companion object {

        val bytesAdapter =
                BytesAdapter.stateless(::LocaleStructureValidatingError)

    }

}

fun Locale.Companion.parser(
        separator: LocaleSeparator = LocaleSeparator.default
): (String) -> Locale {

    val structoreErrorException = LocaleStructureValidatingError().exception

    fun List<String>.toLocale(): Locale {

        (size <= 2)
                .ifFalse { throw structoreErrorException }

        (size == 1 && get(0).isEmpty())
                .ifTrue { return Locale.Empty }

        val language = getOrNull(0)
                .elvis { return Locale.Empty }
                .let(::LocaleLanguage)

        val region = getOrNull(1)
                .elvis { return Locale.Language(language) }
                .let(::LocaleRegion)

        return Locale.LanguageWithRegion(language, region)

    }

    return { encodedLocale ->
        encodedLocale.split(separator.value).toLocale()
    }

}