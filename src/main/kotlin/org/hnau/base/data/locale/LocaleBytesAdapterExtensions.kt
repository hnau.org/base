package org.hnau.base.data.locale

import org.hnau.base.data.bytes.adapter.BytesAdapter
import org.hnau.base.data.bytes.provider.read
import org.hnau.base.data.bytes.provider.readByte
import org.hnau.base.data.bytes.provider.readString
import org.hnau.base.data.bytes.receiver.write
import org.hnau.base.data.bytes.receiver.writeByte
import org.hnau.base.data.bytes.receiver.writeString
import org.hnau.base.data.mapper.Mapper


private val localeBytesAdapter = BytesAdapter<Locale>(
        write = { locale ->
            when (locale) {
                Locale.Empty -> writeByte(0)
                is Locale.Language -> {
                    writeByte(1)
                    write(locale.language, LocaleLanguage.bytesAdapter.write)
                }
                is Locale.LanguageWithRegion -> {
                    writeByte(2)
                    write(locale.language, LocaleLanguage.bytesAdapter.write)
                    write(locale.region, LocaleRegion.bytesAdapter.write)
                }
            }
        },
        read = {
            readByte().toInt().let { partsCount ->
                when (partsCount) {
                    0 -> Locale.Empty
                    1 -> Locale.Language(
                            read(LocaleLanguage.bytesAdapter.read)
                    )
                    2 -> Locale.LanguageWithRegion(
                            read(LocaleLanguage.bytesAdapter.read),
                            read(LocaleRegion.bytesAdapter.read)
                    )
                    else -> error("Unexpected Locale parts count $partsCount")
                }
            }
        }
)

val Locale.Companion.bytesAdapter
    get() = localeBytesAdapter