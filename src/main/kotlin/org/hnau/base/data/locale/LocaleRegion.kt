package org.hnau.base.data.locale

import org.hnau.base.data.bytes.adapter.map
import org.hnau.base.data.mapper.Mapper
import org.hnau.base.utils.validator.Validator
import org.hnau.base.utils.validator.and
import org.hnau.base.utils.validator.charsequence.alphabet
import org.hnau.base.utils.validator.size.length


data class LocaleRegion(
        val value: String
) {

    companion object {

        const val length = 2

        private val validator =
                Validator.length(length) and Validator.alphabet(('A'..'Z').toSet())

        val stringToLocaleRegionMapper =
                Mapper(::LocaleRegion, LocaleRegion::value)

        val bytesAdapter =
                LocalePartsUtils.twoBytesStringBytesAdapter.map(stringToLocaleRegionMapper)

    }

    init {
        validator.validate(value)
    }

    override fun toString() = value

}