package org.hnau.base.data.locale

import org.hnau.base.data.bytes.adapter.BytesAdapter
import org.hnau.base.data.bytes.provider.readByte
import org.hnau.base.data.bytes.receiver.writeByte


object LocalePartsUtils {

    val twoBytesStringBytesAdapter = BytesAdapter(
            read = {
                val bytes = byteArrayOf(readByte(), readByte())
                String(bytes, Charsets.US_ASCII)
            },
            write = {
                val bytes = it.toByteArray(Charsets.US_ASCII)
                writeByte(bytes[0])
                writeByte(bytes[1])
            }
    )

}