package org.hnau.base.data.hashed

import java.security.MessageDigest


private val digest: MessageDigest =
        MessageDigest.getInstance("SHA-256")

@Suppress("DEPRECATION")
fun Hashed.Companion.fromString(
        string: String
) = Hashed(
        digest.digest(string.toByteArray())
)