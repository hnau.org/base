package org.hnau.base.data.hashed

import org.hnau.base.data.bytes.adapter.BytesAdapter
import org.hnau.base.data.bytes.provider.readByteArray
import org.hnau.base.data.bytes.receiver.writeByteArray
import org.hnau.base.data.mapper.Mapper
import org.hnau.base.data.mapper.plus
import org.hnau.base.utils.base64.Base64Type
import org.hnau.base.utils.validator.Validator
import org.hnau.base.utils.validator.size.byteArraySize


data class Hashed
@Deprecated("Use Hashed.fromString(String) instead")
constructor(
        val value: ByteArray
) {

    companion object {

        val validator =
                Validator.byteArraySize(32)

        const val encodedLength = 44

        @Suppress("DEPRECATION")
        val bytesToHashedMapper =
                Mapper(::Hashed, Hashed::value)

        val stringToHashedMapper =
                Base64Type.urlSafe.base64StringToBytesMapper + bytesToHashedMapper

        @Suppress("DEPRECATION")
        val bytesAdapter = BytesAdapter<Hashed>(
                read = { Hashed(readByteArray()) },
                write = { writeByteArray(it.value) }
        )

    }

    init {
        validator.validate(value)
    }

    override fun equals(other: Any?): Boolean {
        val otherHashed = other as? Hashed ?: return false
        return value contentEquals otherHashed.value
    }

    override fun toString() = Base64Type.urlSafe.encode(value)

}