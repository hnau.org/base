package org.hnau.base.data.type


data class Type<T>(
        val clazz: Class<T>,
        val nullable: Boolean
) {

    companion object

}