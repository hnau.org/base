package org.hnau.base.data.type

import org.hnau.base.extensions.isNullable


@Suppress("FunctionName")
inline fun <reified T> Type() = Type(
        clazz = T::class.java,
        nullable = isNullable<T>()
)