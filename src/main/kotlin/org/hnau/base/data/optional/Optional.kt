package org.hnau.base.data.optional


sealed class Optional<out T> {

    companion object;

    object None : Optional<Nothing>()

    data class Some<T>(
            val value: T
    ) : Optional<T>()

}