package org.hnau.base.data.optional

import org.hnau.base.extensions.it


inline fun <I, O> Optional<I>.map(
        transform: (I) -> O
) = checkOptional(
        ifSome = { Optional.Some(transform(it)) },
        ifNone = { Optional.None }
)

fun <T> Optional<Optional<T>>.collapse() = checkOptional(
        ifSome = ::it,
        ifNone = { Optional.None }
)

inline fun <A, B, R> Optional.Companion.combine(
        first: Optional<A>,
        second: Optional<B>,
        combinator: (A, B) -> R
) = first.checkOptional(
        ifSome = { firstValue ->
            second.map { secondValue -> combinator(firstValue, secondValue) }
        },
        ifNone = { Optional.None }
)