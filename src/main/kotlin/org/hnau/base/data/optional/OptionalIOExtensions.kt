package org.hnau.base.data.optional

import org.hnau.base.extensions.boolean.checkBoolean
import org.hnau.base.extensions.checkNullable
import org.hnau.base.extensions.it
import java.lang.IllegalStateException


inline fun <T, R> Optional<T>.checkOptional(
        ifSome: (T) -> R,
        ifNone: () -> R
) = when (this) {
    Optional.None -> ifNone()
    is Optional.Some -> ifSome(value)
}

inline fun <T> Optional<T>.valueOrElse(
        ifNone: () -> T
) = checkOptional(
        ifSome = ::it,
        ifNone = ifNone
)

inline fun <T> Optional<T>.elvis(
        ifNone: () -> Optional<T>
) = checkOptional(
        ifSome = { this },
        ifNone = ifNone
)

val <T> Optional<T>.valueOrNull
    get() = valueOrElse { null }

val <T> Optional<T>.valueOrThrow
    get() = valueOrElse { throw IllegalStateException("Optional is None") }

fun <T> Optional.Companion.fromNullable(
        nullable: T?
) = nullable.checkNullable(
        ifNull = { Optional.None },
        ifNotNull = { Optional.Some(it) }
)

val <T> T?.asNullableToOptional
    get() = Optional.fromNullable(this)

fun <K, V> Map<K, V>.getOptional(
        key: K
) = get(key).checkNullable(
        ifNull = {
            containsKey(key).checkBoolean(
                    ifTrue = {
                        @Suppress("UNCHECKED_CAST")
                        Optional.Some(null as V)
                    },
                    ifFalse = { Optional.None }
            )
        },
        ifNotNull = { existenceValue ->
            Optional.Some(existenceValue)
        }
)