package org.hnau.base.data.delegate.mutable

import org.hnau.base.data.possible.lateinit.Lateinit
import org.hnau.base.data.possible.lateinit.synchronized
import org.hnau.base.data.possible.valueOrThrow


inline fun <T> MutableDelegate.Companion.create(
        crossinline get: () -> T,
        crossinline set: (value: T) -> Unit
) = object : MutableDelegate<T> {

    override fun set(value: T) = set.invoke(value)

    override fun get() = get.invoke()

}

fun <T> MutableDelegate.Companion.cache() =
        object : MutableDelegate<T> {

            private val value = Lateinit.synchronized<T>()

            override fun set(value: T) =
                    this.value.set(value)

            override fun get() =
                    value.valueOrThrow

        }