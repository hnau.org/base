package org.hnau.base.data.delegate

import kotlin.reflect.KProperty
import kotlin.reflect.KProperty0
import kotlin.reflect.KProperty1
import kotlin.reflect.KProperty2


@Suppress("NO_REFLECTION_IN_CLASS_PATH")
fun <T> KProperty<T>.toDelegate() =
        Delegate.create { getter.call() }

@Suppress("NO_REFLECTION_IN_CLASS_PATH")
fun <T> KProperty0<T>.toDelegate() =
        Delegate.create(::get)

@Suppress("NO_REFLECTION_IN_CLASS_PATH")
fun <R, T> KProperty1<R, T>.toDelegate(receiver: R) =
        Delegate.create { get(receiver) }

@Suppress("NO_REFLECTION_IN_CLASS_PATH")
fun <R1, R2, T> KProperty2<R1, R2, T>.toDelegate(receiver1: R1, receiver2: R2) =
        Delegate.create { get(receiver1, receiver2) }