package org.hnau.base.data.delegate


inline fun <I, O> Delegate<I>.map(crossinline convert: (I) -> O) =
        Delegate.create { get().let(convert) }