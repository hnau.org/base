package org.hnau.base.data.delegate.mutable

import org.hnau.base.data.delegate.Delegate
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty


interface MutableDelegate<T> : Delegate<T>, ReadWriteProperty<Any?, T> {

    companion object;

    fun set(value: T)

    override fun setValue(
            thisRef: Any?,
            property: KProperty<*>,
            value: T
    ) = set(
            value = value
    )

    override fun get(): T

    override fun getValue(thisRef: Any?, property: KProperty<*>) =
            super<Delegate>.getValue(thisRef, property)


}