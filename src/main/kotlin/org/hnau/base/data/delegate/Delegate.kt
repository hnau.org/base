package org.hnau.base.data.delegate

import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty


interface Delegate<out T> : ReadOnlyProperty<Any?, T> {

    companion object;

    fun get(): T

    override fun getValue(thisRef: Any?, property: KProperty<*>) = get()

}