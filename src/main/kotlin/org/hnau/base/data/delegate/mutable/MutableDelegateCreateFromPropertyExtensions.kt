package org.hnau.base.data.delegate.mutable

import kotlin.reflect.KMutableProperty
import kotlin.reflect.KMutableProperty0
import kotlin.reflect.KMutableProperty1
import kotlin.reflect.KMutableProperty2


@Suppress("NO_REFLECTION_IN_CLASS_PATH")
fun <T> KMutableProperty<T>.toDelegate() = MutableDelegate.create(
        get = { getter.call() },
        set = { value -> setter.call(value) }
)

@Suppress("NO_REFLECTION_IN_CLASS_PATH")
fun <T> KMutableProperty0<T>.toDelegate() = MutableDelegate.create(
        get = ::get,
        set = ::set
)

@Suppress("NO_REFLECTION_IN_CLASS_PATH")
fun <R, T> KMutableProperty1<R, T>.toDelegate(
        receiver: R
) = MutableDelegate.create(
        get = { get(receiver) },
        set = { value -> set(receiver, value) }
)

@Suppress("NO_REFLECTION_IN_CLASS_PATH")
fun <R1, R2, T> KMutableProperty2<R1, R2, T>.toDelegate(
        receiver1: R1,
        receiver2: R2
) = MutableDelegate.create(
        get = { get(receiver1, receiver2) },
        set = { value -> set(receiver1, receiver2, value) }
)