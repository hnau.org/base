package org.hnau.base.data.delegate.mutable

import org.hnau.base.data.mapper.Mapper


fun <I, O> MutableDelegate<I>.map(
        mapper: Mapper<I, O>
) = MutableDelegate.create(
        get = { get().let(mapper.direct) },
        set = { value -> set(mapper.reverse(value)) }
)