package org.hnau.base.data.delegate


inline fun <T> Delegate.Companion.create(
        crossinline get: () -> T
) = object : Delegate<T> {

    override fun get() = get.invoke()

}