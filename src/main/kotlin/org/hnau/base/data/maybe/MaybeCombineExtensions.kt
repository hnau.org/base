package org.hnau.base.data.maybe


inline fun <A, B, R> Maybe.Companion.combine(
        first: Maybe<A>,
        second: Maybe<B>,
        combinator: (A, B) -> R
) = first
        .map { a ->
            second.map { b ->
                combinator(a, b)
            }
        }
        .collapse()

inline fun <T, O, R> Maybe<T>.combineWith(
        other: Maybe<O>,
        combinator: (T, O) -> R
) = Maybe.combine(
        first = this,
        second = other,
        combinator = combinator
)