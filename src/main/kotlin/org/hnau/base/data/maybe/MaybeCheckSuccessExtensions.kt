package org.hnau.base.data.maybe

import org.hnau.base.extensions.it

val Maybe<*>.isSuccess
    get() = checkMaybe(ifSuccess = { true }, ifError = { false })

inline fun <T> Maybe<T>.valueOrElse(
        ifError: (Throwable) -> T
) = checkMaybe(
        ifSuccess = ::it,
        ifError = ifError
)

inline fun <T> Maybe<T>.errorOrElse(
        ifSuccess: (T) -> Throwable
) = checkMaybe(
        ifSuccess = ifSuccess,
        ifError = ::it
)

val <T> Maybe<T>.valueOrNull
    get() = checkMaybe(
            ifSuccess = ::it,
            ifError = { null }
    )

val <T> Maybe<T>.errorOrNull
    get() = checkMaybe(
            ifSuccess = { null },
            ifError = ::it
    )

val <T> Maybe<T>.valueOrThrow
    get() = valueOrElse { error("There is no value: is error") }

val <T> Maybe<T>.errorOrThrow
    get() = errorOrElse { error("There is no error: is success") }