package org.hnau.base.data.maybe


inline fun <T, R> Maybe<T>.checkMaybe(
        ifSuccess: (T) -> R,
        ifError: (Throwable) -> R
) = when (this) {
    is Maybe.Success -> ifSuccess(value)
    is Maybe.Error -> ifError(throwable)
}