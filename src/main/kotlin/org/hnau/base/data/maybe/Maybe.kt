package org.hnau.base.data.maybe


sealed class Maybe<out T> {

    data class Success<out T> @Deprecated("Use Maybe.success instead") constructor(
            val value: T
    ) : Maybe<T>()

    data class Error @Deprecated("Use Maybe.error instead") constructor(
            val throwable: Throwable
    ) : Maybe<Nothing>()


    companion object

}