package org.hnau.base.data.maybe

import org.hnau.base.extensions.checkNullable


inline fun <T> Maybe.Companion.notNullOrError(
        valueOrNull: T?,
        ifNull: () -> Maybe<T> = { error(IllegalStateException("valueOrNull is null")) }
) = valueOrNull.checkNullable(
        ifNotNull = this::success,
        ifNull = ifNull
)

inline fun <T> T?.notNullOrError(
        ifNull: () -> Maybe<T> = { Maybe.error(IllegalStateException("valueOrNull is null")) }
) = Maybe.notNullOrError(
        valueOrNull = this,
        ifNull = ifNull
)