package org.hnau.base.data.maybe

import org.hnau.base.utils.tryOrElse


@Suppress("DEPRECATION")
fun <T> Maybe.Companion.success(
        value: T
) = Maybe.Success(
        value = value
)

@Suppress("DEPRECATION")
fun Maybe.Companion.error(
        throwable: Throwable
) = Maybe.Error(
        throwable = throwable
)

inline fun <T> tryOrError(
        onThrow: (th: Throwable) -> Unit = {},
        finally: () -> Unit = {},
        action: () -> T
) = tryOrElse(
        action = { Maybe.success(action()) },
        onThrow = { th ->
            onThrow(th)
            Maybe.error(th)
        },
        finally = finally
)