package org.hnau.base.data.maybe

import org.hnau.base.extensions.it


inline fun <T, R> Maybe<T>.ifSuccess(action: (T) -> R) = checkMaybe(action, { null })
inline fun <T, R> Maybe<T>.ifError(action: (Throwable) -> R) = checkMaybe({ null }, action)

fun <T> Maybe<T>.takeIfSuccess() = ifSuccess(::it)
fun <T> Maybe<T>.takeIfError() = ifError(::it)