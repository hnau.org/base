package org.hnau.base.data.maybe

inline fun <T, R> Maybe<T>.map(converter: (T) -> R) = checkMaybe(
        ifSuccess = { value -> Maybe.success(converter(value)) },
        ifError = Maybe.Companion::error
)

inline fun <T> Maybe<T>.mapError(converter: (Throwable) -> Throwable) = checkMaybe(
        ifSuccess = Maybe.Companion::success,
        ifError = { value -> Maybe.error(converter(value)) }
)