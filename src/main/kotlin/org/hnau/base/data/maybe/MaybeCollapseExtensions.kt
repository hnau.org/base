package org.hnau.base.data.maybe

import org.hnau.base.extensions.it


fun <T> Maybe<Maybe<T>>.collapse() = checkMaybe(
        ifSuccess = ::it,
        ifError = Maybe.Companion::error
)