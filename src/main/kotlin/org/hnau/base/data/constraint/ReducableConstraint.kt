package org.hnau.base.data.constraint

import org.hnau.base.data.optional.Optional
import org.hnau.base.data.single.Single


interface ReducableConstraint<C> {

    fun tryReduce(): Optional<C>

}