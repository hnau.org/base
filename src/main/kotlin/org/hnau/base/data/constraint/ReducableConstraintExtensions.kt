package org.hnau.base.data.constraint

import org.hnau.base.data.optional.Optional
import org.hnau.base.data.optional.checkOptional
import org.hnau.base.data.optional.collapse
import org.hnau.base.data.optional.map
import org.hnau.base.data.single.Single
import org.hnau.base.extensions.elvis
import org.hnau.base.extensions.ifNotNull


fun <C : ReducableConstraint<C>, R> C.reduceUntilSuitable(
        tryExtractResult: (C) -> Optional<R>
): Optional<R> = tryExtractResult(this).elvis {
    tryReduce()
            .map { it.reduceUntilSuitable(tryExtractResult) }
            .collapse()
}

val <C : ReducableConstraint<C>, R> ((C) -> Optional<R>).constraintReducer: (C) -> Optional<R>
    get() = { constraint -> constraint.reduceUntilSuitable(this) }