package org.hnau.base.data.tree


abstract class PathProvider<P, T> {

    abstract operator fun get(path: Iterable<P>): T

    operator fun invoke() = get(emptyList())

    protected fun createNextPathBuilder(
            pathIdentifier: P
    ): (Iterable<P>) -> Iterable<P> = listOf(pathIdentifier).let { prefix ->
        { path -> prefix + path }
    }

}