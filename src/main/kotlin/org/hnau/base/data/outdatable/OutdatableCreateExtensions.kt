package org.hnau.base.data.outdatable

import org.hnau.base.data.time.Time
import org.hnau.base.data.time.now


@Suppress("DEPRECATION")
fun <T> Outdatable.Companion.till(
        value: T,
        actualTill: Time?
) = Outdatable(
        value = value,
        actualTill = actualTill
)


fun <T> Outdatable.Companion.since(
        value: T,
        lifetime: Time?,
        since: Time = Time.now()
) = till(
        value = value,
        actualTill = lifetime?.plus(since)
)

fun <T> Outdatable.Companion.eternal(value: T) =
        till(value, null)