package org.hnau.base.data.outdatable

import org.hnau.base.extensions.it


inline fun <T> Outdatable<T>.actualOrElse(
        ifOutdated: () -> T
) = checkActual(
        ifActual = ::it,
        ifOutdated = ifOutdated
)

inline fun <T, R> Outdatable<T>.ifActual(
        ifActual: (T) -> R
) = checkActual(
        ifActual = ifActual,
        ifOutdated = { null }
)

fun <T, R> Outdatable<T>.takeIfActual() = ifActual(::it)

inline fun <T, R> Outdatable<T>.ifOutdated(
        ifOutdated: () -> R
) = checkActual(
        ifActual = { null },
        ifOutdated = ifOutdated
)