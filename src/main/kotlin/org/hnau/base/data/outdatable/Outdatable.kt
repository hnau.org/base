package org.hnau.base.data.outdatable

import org.hnau.base.data.time.Time
import org.hnau.base.extensions.boolean.checkBoolean
import org.hnau.base.data.time.now


class Outdatable<out T> @PublishedApi internal constructor(
        @PublishedApi
        internal val value: T,
        @PublishedApi
        internal val actualTill: Time? = null
) {

    companion object

    val forceValue
        get() = value

    @PublishedApi
    internal val isActual
        get() = actualTill != null && actualTill >= Time.now()

    inline fun <R> checkActual(
            ifActual: (T) -> R,
            ifOutdated: () -> R
    ) = isActual.checkBoolean(
            ifTrue = { ifActual(value) },
            ifFalse = ifOutdated
    )

}