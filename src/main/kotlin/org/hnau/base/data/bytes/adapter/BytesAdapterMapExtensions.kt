package org.hnau.base.data.bytes.adapter

import org.hnau.base.data.mapper.Mapper


fun <I, O> BytesAdapter<I>.map(
        mapper: Mapper<I, O>
) = BytesAdapter<O>(
        read = { mapper.direct(read()) },
        write = { write(mapper.reverse(it)) }
)