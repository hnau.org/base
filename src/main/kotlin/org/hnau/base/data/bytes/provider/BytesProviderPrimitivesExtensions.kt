package org.hnau.base.data.bytes.provider

import org.hnau.base.extensions.boolean.toBoolean

fun BytesProvider.readByte() = read()

private fun BytesProvider.readByteAsInt() =
        readByte().toInt() and 0xFF

fun BytesProvider.readShort(): Short {

    var result = 0
    repeat(Short.SIZE_BYTES) {
        result = (result shl 8) or readByteAsInt()
    }
    return result.toShort()
}

fun BytesProvider.readChar() =
        readShort().toChar()

fun BytesProvider.readInt(): Int {
    var result = 0
    repeat(Int.SIZE_BYTES) {
        result = (result shl 8) or readByteAsInt()
    }
    return result
}

fun BytesProvider.readLong(): Long {

    fun BytesProvider.readByteAsLong() =
            readByte().toLong() and 0xFFL

    var result = 0L
    repeat(Long.SIZE_BYTES) {
        result = (result shl 8) or readByteAsLong()
    }
    return result
}

fun BytesProvider.readFloat() =
        Float.fromBits(readInt())

fun BytesProvider.readDouble() =
        Double.fromBits(readLong())

fun BytesProvider.readBoolean() =
        readByte().toBoolean()