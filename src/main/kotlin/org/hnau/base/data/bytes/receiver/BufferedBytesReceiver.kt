package org.hnau.base.data.bytes.receiver


class BufferedBytesReceiver : BytesReceiver {

    private val bufferInner = ArrayList<Byte>()

    val buffer
        get() = bufferInner.toByteArray()

    override fun invoke(p1: Byte) {
        bufferInner.add(p1)
    }

}