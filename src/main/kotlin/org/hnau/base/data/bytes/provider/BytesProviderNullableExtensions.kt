package org.hnau.base.data.bytes.provider

import org.hnau.base.data.bytes.BytesIO
import org.hnau.base.extensions.boolean.checkBoolean


inline fun <T> BytesProvider.readNullable(
        read: BytesProvider.() -> T
) = readBoolean().checkBoolean(
        ifTrue = { read() },
        ifFalse = { null }
)

inline fun <T> BytesIO.readNullable(
        crossinline read: BytesProvider.() -> T
): BytesProvider.() -> T? = {
    readNullable(read)
}

val <T> (BytesProvider.() -> T).nullable
    get() = BytesIO.readNullable(this)