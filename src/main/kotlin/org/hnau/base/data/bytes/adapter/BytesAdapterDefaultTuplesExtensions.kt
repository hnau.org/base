package org.hnau.base.data.bytes.adapter

import org.hnau.base.data.bytes.BytesIO
import org.hnau.base.data.bytes.provider.readPair
import org.hnau.base.data.bytes.provider.readSingle
import org.hnau.base.data.bytes.provider.readTriple
import org.hnau.base.data.bytes.receiver.writePair
import org.hnau.base.data.bytes.receiver.writeSingle
import org.hnau.base.data.bytes.receiver.writeTriple
import org.hnau.base.data.single.Single


fun <T> BytesAdapter.Companion.single(
        valueBytesAdapter: BytesAdapter<T>
) = BytesAdapter<Single<T>>(
        read = BytesIO.readSingle(valueBytesAdapter.read),
        write = BytesIO.writeSingle(valueBytesAdapter.write)
)

fun <A, B> BytesAdapter.Companion.pair(
        firstBytesAdapter: BytesAdapter<A>,
        secondBytesAdapter: BytesAdapter<B>
) = BytesAdapter<Pair<A, B>>(
        read = BytesIO.readPair(firstBytesAdapter.read, secondBytesAdapter.read),
        write = BytesIO.writePair(firstBytesAdapter.write, secondBytesAdapter.write)
)

fun <A, B, C> BytesAdapter.Companion.triple(
        firstBytesAdapter: BytesAdapter<A>,
        secondBytesAdapter: BytesAdapter<B>,
        thirdBytesAdapter: BytesAdapter<C>
) = BytesAdapter<Triple<A, B, C>>(
        read = BytesIO.readTriple(firstBytesAdapter.read, secondBytesAdapter.read, thirdBytesAdapter.read),
        write = BytesIO.writeTriple(firstBytesAdapter.write, secondBytesAdapter.write, thirdBytesAdapter.write)
)