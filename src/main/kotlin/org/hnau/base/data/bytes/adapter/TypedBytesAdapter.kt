package org.hnau.base.data.bytes.adapter

import org.hnau.base.data.bytes.BytesIO
import org.hnau.base.data.bytes.provider.BytesProvider
import org.hnau.base.data.bytes.provider.readTyped
import org.hnau.base.data.bytes.receiver.TypedWriter
import org.hnau.base.data.bytes.receiver.toTypedWriter
import org.hnau.base.data.bytes.receiver.writeTyped


data class TypedBytesAdapter<T>(
        val writer: TypedWriter<T>,
        val read: BytesProvider.() -> T
)

inline fun <reified E : Any> BytesAdapter<E>.toTypedAdapter() =
        TypedBytesAdapter<E>(
                writer = write.toTypedWriter(),
                read = read
        )

val <K, T : Any> Map<K, TypedBytesAdapter<out T>>.readers: Map<K, BytesProvider.() -> T>
    get() = mapValues { (_, value) -> value.read }

val <K, T : Any> Map<K, TypedBytesAdapter<out T>>.writers: Iterable<Pair<K, TypedWriter<out T>>>
    get() = map { (key, value) -> key to value.writer }

fun <K, T : Any> BytesAdapter.Companion.typed(
        typedAdapters: Map<K, TypedBytesAdapter<out T>>,
        keyAdapter: BytesAdapter<K>
) = BytesAdapter(
        read = BytesIO.readTyped<K, T>(
                typedReaders = typedAdapters.readers,
                readKey = keyAdapter.read
        ),
        write = BytesIO.writeTyped<K, T>(
                typedWriters = typedAdapters.writers,
                writeKey = keyAdapter.write
        )
)