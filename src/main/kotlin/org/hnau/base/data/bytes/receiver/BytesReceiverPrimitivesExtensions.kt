package org.hnau.base.data.bytes.receiver

import org.hnau.base.extensions.boolean.toByte


fun BytesReceiver.writeByte(byte: Byte) =
        write(byte)

private val Int.lastByte
    get() = (this and 0xFF).toByte()

fun BytesReceiver.writeShort(short: Short) {
    val int = short.toInt()
    write((int ushr 8).lastByte)
    write((int ushr 0).lastByte)
}

fun BytesReceiver.writeChar(char: Char) =
        writeShort(char.toShort())

fun BytesReceiver.writeInt(int: Int) {
    write((int ushr 24).lastByte)
    write((int ushr 16).lastByte)
    write((int ushr 8).lastByte)
    write((int ushr 0).lastByte)
}

private val Long.lastByte
    get() = (this and 0xFF).toByte()

fun BytesReceiver.writeLong(long: Long) {
    write((long ushr 56).lastByte)
    write((long ushr 48).lastByte)
    write((long ushr 40).lastByte)
    write((long ushr 32).lastByte)
    write((long ushr 24).lastByte)
    write((long ushr 16).lastByte)
    write((long ushr 8).lastByte)
    write((long ushr 0).lastByte)
}

fun BytesReceiver.writeFloat(float: Float) =
        writeInt(float.toBits())

fun BytesReceiver.writeDouble(double: Double) =
        writeLong(double.toBits())

fun BytesReceiver.writeBoolean(boolean: Boolean) =
        writeByte(boolean.toByte())