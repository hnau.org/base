package org.hnau.base.data.bytes.receiver

import org.hnau.base.data.bytes.BytesIO
import org.hnau.base.extensions.checkNullable


inline fun <T> BytesReceiver.writeNullable(
        value: T?,
        write: BytesReceiver.(T) -> Unit
) = value.checkNullable(
        ifNotNull = { existenceValue ->
            writeBoolean(true)
            write(this, existenceValue)
        },
        ifNull = {
            writeBoolean(false)
        }
)

inline fun <T> BytesIO.writeNullable(
        crossinline write: BytesReceiver.(T) -> Unit
): BytesReceiver.(T?) -> Unit = {
    writeNullable(it, write)
}

val <T> (BytesReceiver.(T) -> Unit).nullable
    get() = BytesIO.writeNullable(this)