package org.hnau.base.data.bytes.provider

import java.nio.charset.Charset


fun BytesProvider.readString(
        charset: Charset
) = String(
        bytes = readByteArray(),
        charset = charset
)

fun BytesProvider.readString() = readString(
        charset = Charsets.UTF_8
)
