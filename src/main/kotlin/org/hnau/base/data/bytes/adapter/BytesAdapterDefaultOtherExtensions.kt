package org.hnau.base.data.bytes.adapter

import org.hnau.base.data.bytes.provider.readString
import org.hnau.base.data.bytes.receiver.writeString
import java.nio.charset.Charset

fun BytesAdapter.Companion.string(
        charset: Charset = Charsets.UTF_8
) = BytesAdapter(
        read = { readString(charset) },
        write = { writeString(it, charset) }
)