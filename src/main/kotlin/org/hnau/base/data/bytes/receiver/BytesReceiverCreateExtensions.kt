package org.hnau.base.data.bytes.receiver

import java.io.OutputStream


fun OutputStream.toBytesReceiver(): BytesReceiver = { write(it.toInt()) }