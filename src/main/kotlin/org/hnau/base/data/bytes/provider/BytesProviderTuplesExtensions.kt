package org.hnau.base.data.bytes.provider

import org.hnau.base.data.bytes.BytesIO
import org.hnau.base.data.single.Single


inline fun <T> BytesProvider.readSingle(
        read: BytesProvider.() -> T
) = Single(
        value = read()
)

inline fun <T> BytesIO.readSingle(
        crossinline read: BytesProvider.() -> T
): BytesProvider.() -> Single<T> = {
    readSingle(read)
}

inline fun <A, B> BytesProvider.readPair(
        readFirst: BytesProvider.() -> A,
        readSecond: BytesProvider.() -> B
) = Pair(
        readFirst(),
        readSecond()
)

inline fun <A, B> BytesIO.readPair(
        crossinline readFirst: BytesProvider.() -> A,
        crossinline readSecond: BytesProvider.() -> B
): BytesProvider.() -> Pair<A, B> = {
    readPair(readFirst, readSecond)
}

inline fun <A, B, C> BytesProvider.readTriple(
        readFirst: BytesProvider.() -> A,
        readSecond: BytesProvider.() -> B,
        readThird: BytesProvider.() -> C
) = Triple(
        readFirst(),
        readSecond(),
        readThird()
)

inline fun <A, B, C> BytesIO.readTriple(
        crossinline readFirst: BytesProvider.() -> A,
        crossinline readSecond: BytesProvider.() -> B,
        crossinline readThird: BytesProvider.() -> C
): BytesProvider.() -> Triple<A, B, C> = {
    readTriple(readFirst, readSecond, readThird)
}