package org.hnau.base.data.bytes.receiver

import org.hnau.base.data.bytes.BytesIO
import org.hnau.base.extensions.cast


data class TypedWriter<T>(
        val type: Class<T>,
        val write: BytesReceiver.(T) -> Unit
)

inline fun <reified T> TypedWriter(
        noinline write: BytesReceiver.(T) -> Unit
) = TypedWriter(
        type = T::class.java,
        write = write
)

inline fun <reified T> (BytesReceiver.(T) -> Unit).toTypedWriter() =
        TypedWriter(this)

fun <K, T : Any> BytesIO.writeTyped(
        typedWriters: Iterable<Pair<K, TypedWriter<out T>>>,
        writeKey: BytesReceiver.(K) -> Unit
): BytesReceiver.(T) -> Unit {

    val typesBytesAdaptersByClass = typedWriters.associate { (key, typedWriter) ->
        typedWriter.type to (key to typedWriter)
    }

    fun <TT : T> BytesReceiver.write(
            value: TT
    ) {
        val valueClass = value.javaClass
        val (key, typedWriter) = typesBytesAdaptersByClass[valueClass]
                ?: error("There is no writer for class $valueClass")
        writeKey(key)
        typedWriter.cast<TypedWriter<TT>>().write(this, value)
    }

    return BytesReceiver::write
}