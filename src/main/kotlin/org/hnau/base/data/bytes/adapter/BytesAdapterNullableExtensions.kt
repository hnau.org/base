package org.hnau.base.data.bytes.adapter

import org.hnau.base.data.bytes.provider.nullable
import org.hnau.base.data.bytes.receiver.nullable


fun <T> BytesAdapter.Companion.nullable(
        notNull: BytesAdapter<T>
) = BytesAdapter(
        read = notNull.read.nullable,
        write = notNull.write.nullable
)

val <T> BytesAdapter<T>.nullable
    get() = BytesAdapter.nullable(this)