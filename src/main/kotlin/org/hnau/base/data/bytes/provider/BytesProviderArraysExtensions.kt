package org.hnau.base.data.bytes.provider

import org.hnau.base.data.bytes.BytesIO


inline fun <reified T> BytesProvider.readArray(
        read: BytesProvider.() -> T
) = Array<T>(
        size = readInt()
) { read.invoke(this) }

inline fun <reified T> BytesIO.readArray(
        crossinline read: BytesProvider.() -> T
): BytesProvider.() -> Array<T> = {
    readArray(read)
}

fun BytesProvider.readByteArray() =
        ByteArray(readInt()) { readByte() }

fun BytesProvider.readShortArray() =
        ShortArray(readInt()) { readShort() }

fun BytesProvider.readCharArray() =
        CharArray(readInt()) { readChar() }

fun BytesProvider.readIntArray() =
        IntArray(readInt()) { readInt() }

fun BytesProvider.readLongArray() =
        LongArray(readInt()) { readLong() }

fun BytesProvider.readFloatArray() =
        FloatArray(readInt()) { readFloat() }

fun BytesProvider.readDoubleArray() =
        DoubleArray(readInt()) { readDouble() }

fun BytesProvider.readBooleanArray() =
        BooleanArray(readInt()) { readBoolean() }