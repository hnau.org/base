package org.hnau.base.data.bytes.adapter

import org.hnau.base.data.bytes.BytesIO
import org.hnau.base.data.bytes.provider.BytesProvider
import org.hnau.base.data.bytes.provider.readArray
import org.hnau.base.data.bytes.provider.readBooleanArray
import org.hnau.base.data.bytes.provider.readByteArray
import org.hnau.base.data.bytes.provider.readDoubleArray
import org.hnau.base.data.bytes.provider.readFloatArray
import org.hnau.base.data.bytes.provider.readIntArray
import org.hnau.base.data.bytes.provider.readLongArray
import org.hnau.base.data.bytes.provider.readShortArray
import org.hnau.base.data.bytes.receiver.BytesReceiver
import org.hnau.base.data.bytes.receiver.writeArray
import org.hnau.base.data.bytes.receiver.writeBooleanArray
import org.hnau.base.data.bytes.receiver.writeByteArray
import org.hnau.base.data.bytes.receiver.writeDoubleArray
import org.hnau.base.data.bytes.receiver.writeFloatArray
import org.hnau.base.data.bytes.receiver.writeIntArray
import org.hnau.base.data.bytes.receiver.writeLongArray
import org.hnau.base.data.bytes.receiver.writeShortArray


inline fun <reified T> BytesAdapter.Companion.array(
        itemAdapter: BytesAdapter<T>
) = BytesAdapter(
        read = BytesIO.readArray(itemAdapter.read),
        write = BytesIO.writeArray(itemAdapter.write)
)

private val byteArrayBytesAdapter = BytesAdapter(
        read = BytesProvider::readByteArray,
        write = BytesReceiver::writeByteArray
)

val BytesAdapter.Companion.byteArray
    get() = byteArrayBytesAdapter


private val shortArrayBytesAdapter = BytesAdapter(
        read = BytesProvider::readShortArray,
        write = BytesReceiver::writeShortArray
)

val BytesAdapter.Companion.shortArray
    get() = shortArrayBytesAdapter


private val intArrayBytesAdapter = BytesAdapter(
        read = BytesProvider::readIntArray,
        write = BytesReceiver::writeIntArray
)

val BytesAdapter.Companion.intArray
    get() = intArrayBytesAdapter


private val longArrayBytesAdapter = BytesAdapter(
        read = BytesProvider::readLongArray,
        write = BytesReceiver::writeLongArray
)

val BytesAdapter.Companion.longArray
    get() = longArrayBytesAdapter


private val floatArrayBytesAdapter = BytesAdapter(
        read = BytesProvider::readFloatArray,
        write = BytesReceiver::writeFloatArray
)

val BytesAdapter.Companion.floatArray
    get() = floatArrayBytesAdapter


private val doubleArrayBytesAdapter = BytesAdapter(
        read = BytesProvider::readDoubleArray,
        write = BytesReceiver::writeDoubleArray
)

val BytesAdapter.Companion.doubleArray
    get() = doubleArrayBytesAdapter


private val booleanArrayBytesAdapter = BytesAdapter(
        read = BytesProvider::readBooleanArray,
        write = BytesReceiver::writeBooleanArray
)

val BytesAdapter.Companion.booleanArray
    get() = booleanArrayBytesAdapter