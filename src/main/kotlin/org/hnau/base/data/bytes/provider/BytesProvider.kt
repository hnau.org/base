package org.hnau.base.data.bytes.provider


typealias BytesProvider = () -> Byte