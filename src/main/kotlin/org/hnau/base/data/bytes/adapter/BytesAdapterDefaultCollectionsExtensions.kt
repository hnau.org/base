package org.hnau.base.data.bytes.adapter

import org.hnau.base.data.bytes.BytesIO
import org.hnau.base.data.bytes.provider.readList
import org.hnau.base.data.bytes.receiver.writeCollection

fun <T> BytesAdapter.Companion.list(
        itemAdapter: BytesAdapter<T>
) = BytesAdapter(
        read = BytesIO.readList(itemAdapter.read),
        write = BytesIO.writeCollection(itemAdapter.write)
)

