package org.hnau.base.data.bytes.receiver




fun BytesReceiver.write(byte: Byte) = invoke(byte)

inline fun <T> BytesReceiver.write(
        value: T,
        write: BytesReceiver.(T) -> Unit
) = write(
        value
)