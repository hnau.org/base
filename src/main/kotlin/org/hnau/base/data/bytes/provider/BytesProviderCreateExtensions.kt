package org.hnau.base.data.bytes.provider

import java.io.InputStream


fun InputStream.toBytesProvider(): BytesProvider = { read().toByte() }