package org.hnau.base.data.bytes.receiver

import java.nio.charset.Charset

fun BytesReceiver.writeString(
        string: String,
        charset: Charset
) = string
        .toByteArray(charset)
        .let(::writeByteArray)

fun BytesReceiver.writeString(
        string: String
) = writeString(
        string = string,
        charset = Charsets.UTF_8
)