package org.hnau.base.data.bytes.adapter

import org.hnau.base.data.bytes.provider.BytesProvider
import org.hnau.base.data.bytes.receiver.BytesReceiver


data class BytesAdapter<T>(
        val read: BytesProvider.() -> T,
        val write: BytesReceiver.(T) -> Unit
) {

    companion object

}