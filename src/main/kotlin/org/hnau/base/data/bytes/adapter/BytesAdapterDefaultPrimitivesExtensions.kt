package org.hnau.base.data.bytes.adapter

import org.hnau.base.data.bytes.provider.BytesProvider
import org.hnau.base.data.bytes.provider.readBoolean
import org.hnau.base.data.bytes.provider.readByte
import org.hnau.base.data.bytes.provider.readChar
import org.hnau.base.data.bytes.provider.readDouble
import org.hnau.base.data.bytes.provider.readFloat
import org.hnau.base.data.bytes.provider.readInt
import org.hnau.base.data.bytes.provider.readLong
import org.hnau.base.data.bytes.provider.readShort
import org.hnau.base.data.bytes.receiver.BytesReceiver
import org.hnau.base.data.bytes.receiver.writeBoolean
import org.hnau.base.data.bytes.receiver.writeByte
import org.hnau.base.data.bytes.receiver.writeChar
import org.hnau.base.data.bytes.receiver.writeDouble
import org.hnau.base.data.bytes.receiver.writeFloat
import org.hnau.base.data.bytes.receiver.writeInt
import org.hnau.base.data.bytes.receiver.writeLong
import org.hnau.base.data.bytes.receiver.writeShort

inline fun <T> BytesAdapter.Companion.stateless(
        crossinline create: () -> T
) = BytesAdapter(
        read = { create() },
        write = {}
)

private val byteBytesAdapter = BytesAdapter(
        read = BytesProvider::readByte,
        write = BytesReceiver::writeByte
)

val BytesAdapter.Companion.byte
    get() = byteBytesAdapter


private val shortBytesAdapter = BytesAdapter(
        read = BytesProvider::readShort,
        write = BytesReceiver::writeShort
)

val BytesAdapter.Companion.short
    get() = shortBytesAdapter


private val charBytesAdapter = BytesAdapter(
        read = BytesProvider::readChar,
        write = BytesReceiver::writeChar
)

val BytesAdapter.Companion.char
    get() = charBytesAdapter


private val intBytesAdapter = BytesAdapter(
        read = BytesProvider::readInt,
        write = BytesReceiver::writeInt
)

val BytesAdapter.Companion.int
    get() = intBytesAdapter


private val longBytesAdapter = BytesAdapter(
        read = BytesProvider::readLong,
        write = BytesReceiver::writeLong
)

val BytesAdapter.Companion.long
    get() = longBytesAdapter


private val floatBytesAdapter = BytesAdapter(
        read = BytesProvider::readFloat,
        write = BytesReceiver::writeFloat
)

val BytesAdapter.Companion.float
    get() = floatBytesAdapter


private val doubleBytesAdapter = BytesAdapter(
        read = BytesProvider::readDouble,
        write = BytesReceiver::writeDouble
)

val BytesAdapter.Companion.double
    get() = doubleBytesAdapter


private val booleanBytesAdapter = BytesAdapter(
        read = BytesProvider::readBoolean,
        write = BytesReceiver::writeBoolean
)

val BytesAdapter.Companion.boolean
    get() = booleanBytesAdapter
