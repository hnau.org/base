package org.hnau.base.data.bytes.provider


fun BytesProvider.read() = invoke()

inline fun <T> BytesProvider.read(read: BytesProvider.() -> T) = read()