package org.hnau.base.data.bytes.receiver

import org.hnau.base.data.bytes.BytesIO
import org.hnau.base.data.single.Single


inline fun <T> BytesReceiver.writeSingle(
        single: Single<T>,
        write: BytesReceiver.(T) -> Unit
) = write(
        single.value
)

inline fun <T> BytesIO.writeSingle(
        crossinline write: BytesReceiver.(T) -> Unit
): BytesReceiver.(Single<T>) -> Unit = { value ->
    writeSingle(value, write)
}

inline fun <A, B> BytesReceiver.writePair(
        pair: Pair<A, B>,
        writeFirst: BytesReceiver.(A) -> Unit,
        writeSecond: BytesReceiver.(B) -> Unit
) {
    writeFirst(pair.first)
    writeSecond(pair.second)
}

inline fun <A, B> BytesIO.writePair(
        crossinline writeFirst: BytesReceiver.(A) -> Unit,
        crossinline writeSecond: BytesReceiver.(B) -> Unit
): BytesReceiver.(Pair<A, B>) -> Unit = { value ->
    writePair(value, writeFirst, writeSecond)
}

inline fun <A, B, C> BytesReceiver.writeTriple(
        triple: Triple<A, B, C>,
        writeFirst: BytesReceiver.(A) -> Unit,
        writeSecond: BytesReceiver.(B) -> Unit,
        writeThird: BytesReceiver.(C) -> Unit
) {
    writeFirst(triple.first)
    writeSecond(triple.second)
    writeThird(triple.third)
}

inline fun <A, B, C> BytesIO.writeTriple(
        crossinline writeFirst: BytesReceiver.(A) -> Unit,
        crossinline writeSecond: BytesReceiver.(B) -> Unit,
        crossinline writeThird: BytesReceiver.(C) -> Unit
): BytesReceiver.(Triple<A, B, C>) -> Unit = { value ->
    writeTriple(value, writeFirst, writeSecond, writeThird)
}