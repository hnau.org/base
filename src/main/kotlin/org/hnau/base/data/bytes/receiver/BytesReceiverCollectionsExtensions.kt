package org.hnau.base.data.bytes.receiver

import org.hnau.base.data.bytes.BytesIO


inline fun <T> BytesReceiver.writeCollection(
        collection: Collection<T>,
        write: BytesReceiver.(T) -> Unit
) {
    writeInt(collection.size)
    collection.forEach { item -> write(item) }
}

inline fun <T> BytesIO.writeCollection(
        crossinline write: BytesReceiver.(T) -> Unit
): BytesReceiver.(Collection<T>) -> Unit = { value ->
    writeCollection(value, write)
}