package org.hnau.base.data.bytes.receiver

import org.hnau.base.data.bytes.BytesIO


inline fun <T> BytesReceiver.writeArray(
        array: Array<T>,
        write: BytesReceiver.(T) -> Unit
) {
    writeInt(array.size)
    array.forEach { item -> write(item) }
}

inline fun <reified T> BytesIO.writeArray(
        crossinline write: BytesReceiver.(T) -> Unit
): BytesReceiver.(Array<T>) -> Unit = { value ->
    writeArray(value, write)
}

fun BytesReceiver.writeByteArray(array: ByteArray) {
    writeInt(array.size)
    array.forEach { item -> writeByte(item) }
}

fun BytesReceiver.writeShortArray(array: ShortArray) {
    writeInt(array.size)
    array.forEach { item -> writeShort(item) }
}

fun BytesReceiver.writeCharArray(array: CharArray) {
    writeInt(array.size)
    array.forEach { item -> writeChar(item) }
}

fun BytesReceiver.writeIntArray(array: IntArray) {
    writeInt(array.size)
    array.forEach { item -> writeInt(item) }
}

fun BytesReceiver.writeLongArray(array: LongArray) {
    writeInt(array.size)
    array.forEach { item -> writeLong(item) }
}

fun BytesReceiver.writeFloatArray(array: FloatArray) {
    writeInt(array.size)
    array.forEach { item -> writeFloat(item) }
}

fun BytesReceiver.writeDoubleArray(array: DoubleArray) {
    writeInt(array.size)
    array.forEach { item -> writeDouble(item) }
}

fun BytesReceiver.writeBooleanArray(array: BooleanArray) {
    writeInt(array.size)
    array.forEach { item -> writeBoolean(item) }
}