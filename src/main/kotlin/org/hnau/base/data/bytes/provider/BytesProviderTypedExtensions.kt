package org.hnau.base.data.bytes.provider

import org.hnau.base.data.bytes.BytesIO


fun <K, T> BytesIO.readTyped(
        typedReaders: Map<K, BytesProvider.() -> T>,
        readKey: BytesProvider.() -> K
): BytesProvider.() -> T = {
    val key = readKey()
    val reader = typedReaders[key]
            ?: error("Unknown key $key of typed reader")
    reader(this)
}