package org.hnau.base.data.bytes.provider

import org.hnau.base.extensions.number.ifNotPositive


class BufferedBytesProvider(
        private val buffer: ByteArray
) : BytesProvider {

    private var cursor = 0

    val available
        get() = buffer.size - cursor

    override fun invoke(): Byte {
        available.ifNotPositive { throw IndexOutOfBoundsException("There are no next byte") }
        return buffer[cursor++]
    }

}