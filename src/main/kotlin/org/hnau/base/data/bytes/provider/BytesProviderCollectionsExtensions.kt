package org.hnau.base.data.bytes.provider

import org.hnau.base.data.bytes.BytesIO


inline fun <T> BytesProvider.readList(
        read: BytesProvider.() -> T
): List<T> = readInt().let { size ->
    ArrayList<T>(size).apply {
        repeat(size) { add(read.invoke(this@readList)) }
    }
}

inline fun <T> BytesIO.readList(
        crossinline read: BytesProvider.() -> T
): BytesProvider.() -> List<T> = {
    readList(read)
}