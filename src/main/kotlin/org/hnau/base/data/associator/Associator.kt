package org.hnau.base.data.associator


interface Associator<K, W, R> {

    companion object

    fun read(key: K): R

    fun write(key: K, value: W)

}