package org.hnau.base.data.associator

import org.hnau.base.data.mapper.Mapper


inline fun <K, IN_W, IN_R, OUT_W, OUT_R> Associator<K, IN_W, IN_R>.map(
        crossinline convertReader: (K, (K) -> IN_R) -> OUT_R,
        crossinline convertWriter: (K, OUT_W, (K, IN_W) -> Unit) -> Unit
) = Associator.create<K, OUT_W, OUT_R>(
        read = { key -> convertReader(key, ::read) },
        write = { key, value -> convertWriter(key, value, ::write) }
)

inline fun <K, IN_W, IN_R, OUT_W, OUT_R> Associator<K, IN_W, IN_R>.map(
        crossinline direct: (IN_R) -> OUT_R,
        crossinline reverse: (OUT_W) -> IN_W
) = map(
        convertReader = { key, get -> direct(get(key)) },
        convertWriter = { key, value: OUT_W, set -> set(key, reverse(value)) }
)

fun <K, I, O> Associator<K, I, I>.map(
        mapper: Mapper<I, O>
) = map(
        direct = mapper.direct,
        reverse = mapper.reverse
)

inline fun <K, W, IN_R, OUT_R> Associator<K, W, IN_R>.mapReader(
        crossinline convertReader: (K, (K) -> IN_R) -> OUT_R
) = map(
        convertReader = convertReader,
        convertWriter = { key, value: W, set -> set(key, value) }
)

inline fun <K, IN_W, OUT_W, R> Associator<K, IN_W, R>.mapWriter(
        crossinline convertWriter: (K, OUT_W, (K, IN_W) -> Unit) -> Unit
) = map(
        convertReader = { key, get -> get(key) },
        convertWriter = convertWriter
)