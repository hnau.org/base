package org.hnau.base.data.associator


operator fun <K, VI, VO> Associator<K, VI, VO>.get(key: K) = read(key)

operator fun <K, VI, VO> Associator<K, VI, VO>.set(key: K, value: VI) = write(key, value)