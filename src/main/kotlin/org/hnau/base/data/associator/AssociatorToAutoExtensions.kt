package org.hnau.base.data.associator

import org.hnau.base.extensions.elvis


fun <K, V> Associator<K, V, V?>.toAutoAssociator(
        calcValue: (K) -> V
): (K) -> V = mapReader { key, get ->
    get(key).elvis {
        calcValue(key).also { newValue -> write(key, newValue) }
    }
}::read