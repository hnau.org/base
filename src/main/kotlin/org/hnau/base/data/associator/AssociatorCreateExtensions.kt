package org.hnau.base.data.associator


inline fun <K, W, R> Associator.Companion.create(
        crossinline read: (key: K) -> R,
        crossinline write: (key: K, value: W) -> Unit
) = object : Associator<K, W, R> {
    override fun read(key: K) = read.invoke(key)
    override fun write(key: K, value: W) = write.invoke(key, value)
}

fun <K, V> MutableMap<K, V>.toAssociator() =
        Associator.create(::get, ::set)

fun <K, V> Associator.Companion.byHashMap() =
        HashMap<K, V>().toAssociator()