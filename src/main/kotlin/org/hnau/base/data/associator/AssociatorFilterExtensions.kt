package org.hnau.base.data.associator


inline fun <K, VI, VO> Associator<K, VI, VO>.filterValues(
        crossinline predicate: (VO) -> Boolean
) = mapReader { key, get ->
    get(key).takeIf(predicate)
}