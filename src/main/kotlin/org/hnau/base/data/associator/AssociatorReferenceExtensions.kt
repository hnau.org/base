package org.hnau.base.data.associator

import java.lang.ref.Reference
import java.lang.ref.SoftReference
import java.lang.ref.WeakReference


inline fun <K, T, R : Reference<T>> Associator<K, R, R?>.resolveReference(
        crossinline createReference: (T) -> R
) = map(
        direct = { reference -> reference?.get() },
        reverse = createReference
)

fun <K, T> Associator<K, WeakReference<T>, WeakReference<T>?>.resolveWeakReference(): Associator<K, T, T?> =
        resolveReference(::WeakReference)

fun <K, T> Associator<K, SoftReference<T>, SoftReference<T>?>.resolveSoftReference(): Associator<K, T, T?> =
        resolveReference(::SoftReference)