package org.hnau.base.data.possible


interface PossibleAsync<T> {

    suspend fun <R> checkPossible(
            ifValueExists: suspend (value: T) -> R,
            ifValueNotExists: suspend () -> R
    ): R

}