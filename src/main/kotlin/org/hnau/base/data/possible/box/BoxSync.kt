package org.hnau.base.data.possible.box

import org.hnau.base.data.possible.MutablePossibleSync
import org.hnau.base.utils.safe.SafeContext
import org.hnau.base.utils.safe.synchronized
import org.hnau.base.utils.safe.unsafe


class BoxSync<T>(
        @PublishedApi
        internal val safeContext: SafeContext
) : Box<T>(), MutablePossibleSync<T> {

    override fun set(value: T) = safeContext.executeSafe { setUnsafe(true, value) }
    fun clear() = safeContext.executeSafe { setUnsafe(false, null) }

    override fun <R> checkPossible(
            ifValueExists: (value: T) -> R,
            ifValueNotExists: () -> R
    ) = safeContext.executeSafe {
        checkValueExistsUnsafe(
                ifExists = ifValueExists,
                ifNotExists = ifValueNotExists
        )
    }

}

fun <T> Box.Companion.sync(
        safeContext: SafeContext
) = BoxSync<T>(
        safeContext = safeContext
)

fun <T> Box.Companion.synchronized() =
        sync<T>(SafeContext.synchronized())

fun <T> Box.Companion.unsafe() =
        sync<T>(SafeContext.unsafe)