package org.hnau.base.data.possible


interface PossibleSync<T> {

    fun <R> checkPossible(
            ifValueExists: (value: T) -> R,
            ifValueNotExists: () -> R
    ): R

}