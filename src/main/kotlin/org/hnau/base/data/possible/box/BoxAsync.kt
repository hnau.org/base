package org.hnau.base.data.possible.box

import org.hnau.base.data.possible.MutablePossibleAsync
import org.hnau.base.utils.safe.async.SafeContextAsync
import org.hnau.base.utils.safe.async.mutex
import org.hnau.base.utils.safe.async.unsafe


class BoxAsync<T>(
        @PublishedApi
        internal val safeContext: SafeContextAsync
) : Box<T>(), MutablePossibleAsync<T> {

    override fun set(value: T) = safeContext.executeSafe { setUnsafe(true, value) }
    fun clear() = safeContext.executeSafe { setUnsafe(false, null) }

    override suspend fun <R> checkPossible(
            ifValueExists: suspend (value: T) -> R,
            ifValueNotExists: suspend () -> R
    ) = safeContext.executeSafeAsync {
        checkValueExistsUnsafe(
                ifExists = { ifValueExists(it) },
                ifNotExists = { ifValueNotExists() }
        )
    }

}

fun <T> Box.Companion.async(
        safeContext: SafeContextAsync
) = BoxAsync<T>(
        safeContext = safeContext
)

fun <T> Box.Companion.mutex() =
        async<T>(SafeContextAsync.mutex())

fun <T> Box.Companion.unsafeAsync() =
        async<T>(SafeContextAsync.unsafe)