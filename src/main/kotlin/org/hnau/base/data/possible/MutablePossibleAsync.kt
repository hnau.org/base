package org.hnau.base.data.possible


interface MutablePossibleAsync<T> : PossibleAsync<T> {

    fun set(value: T)

}

suspend fun <T> MutablePossibleAsync<T>.update(
        createInitialValue: suspend () -> T,
        updateValue: suspend T.() -> T
) = checkPossible(
        ifValueExists = updateValue,
        ifValueNotExists = createInitialValue
).also(::set)

suspend inline fun <T> MutablePossibleAsync<T>.updateMutable(
        crossinline createInitialValue: suspend () -> T,
        crossinline updateValue: suspend T.() -> Unit
) = checkPossible(
        ifValueExists = { value -> value.also { updateValue(it) } },
        ifValueNotExists = { createInitialValue().also(::set) }
)

suspend inline fun <T> MutablePossibleAsync<T>.getOrInitialize(
        crossinline valueToInitializeIfEmpty: suspend () -> T
) = updateMutable(
        createInitialValue = valueToInitializeIfEmpty,
        updateValue = {}
)