package org.hnau.base.data.possible

import org.hnau.base.extensions.it


fun <T> PossibleSync<T>.valueOrElse(
        ifNoValue: () -> T
) = checkPossible(
        ifValueExists = ::it,
        ifValueNotExists = ifNoValue
)

val <T> PossibleSync<T>.valueOrThrow
    get() = valueOrElse { PossibleUtils.throwValueIsNotInitializedException() }