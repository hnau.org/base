package org.hnau.base.data.possible.lateinit

import org.hnau.base.data.possible.MutablePossibleSync
import org.hnau.base.extensions.boolean.checkBoolean
import org.hnau.base.utils.safe.SafeContext
import org.hnau.base.utils.safe.synchronized
import org.hnau.base.utils.safe.unsafe


class LateinitSync<T>(
        @PublishedApi
        internal val safeContext: SafeContext
) : Lateinit<T>(), MutablePossibleSync<T> {

    @Volatile
    @PublishedApi
    internal var valueExists = false

    @Volatile
    @PublishedApi
    internal var value: T? = null

    override fun set(
            value: T
    ) = safeContext.executeSafe {
        this.value = value
        this.valueExists = true
    }

    override fun <R> checkPossible(
            ifValueExists: (value: T) -> R,
            ifValueNotExists: () -> R
    ) = valueExists.checkBoolean(
            ifTrue = {
                @Suppress("UNCHECKED_CAST")
                ifValueExists(value as T)
            },
            ifFalse = {
                safeContext.executeSafe {
                    valueExists.checkBoolean(
                            ifTrue = {
                                @Suppress("UNCHECKED_CAST")
                                ifValueExists(value as T)
                            },
                            ifFalse = { ifValueNotExists() }
                    )
                }
            }
    )

}

fun <T> Lateinit.Companion.sync(
        safeContext: SafeContext
) = LateinitSync<T>(
        safeContext = safeContext
)

fun <T> Lateinit.Companion.synchronized() =
        sync<T>(SafeContext.synchronized())

fun <T> Lateinit.Companion.unsafe() =
        sync<T>(SafeContext.unsafe)