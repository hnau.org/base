package org.hnau.base.data.possible


suspend fun <T> PossibleAsync<T>.valueOrElse(
        ifNoValue: suspend () -> T = { PossibleUtils.throwValueIsNotInitializedException() }
) = checkPossible(
        ifValueExists = { it },
        ifValueNotExists = ifNoValue
)

suspend fun <T> PossibleAsync<T>.valueOrThrow() =
        valueOrElse { PossibleUtils.throwValueIsNotInitializedException() }