package org.hnau.base.data.possible.lateinit

import org.hnau.base.data.possible.MutablePossibleAsync
import org.hnau.base.extensions.boolean.checkBoolean
import org.hnau.base.utils.safe.async.SafeContextAsync
import org.hnau.base.utils.safe.async.mutex
import org.hnau.base.utils.safe.async.unsafe


class LateinitAsync<T>(
        @PublishedApi
        internal val safeContext: SafeContextAsync
) : Lateinit<T>(), MutablePossibleAsync<T> {

    @PublishedApi
    internal var valueExists = false

    @PublishedApi
    internal var value: T? = null

    override fun set(
            value: T
    ) = safeContext.executeSafe {
        this.value = value
        this.valueExists = true
    }

    override suspend fun <R> checkPossible(
            ifValueExists: suspend (value: T) -> R,
            ifValueNotExists: suspend () -> R
    ) = valueExists.checkBoolean(
            ifTrue = {
                @Suppress("UNCHECKED_CAST")
                ifValueExists(value as T)
            },
            ifFalse = {
                safeContext.executeSafeAsync {
                    valueExists.checkBoolean(
                            ifTrue = {
                                @Suppress("UNCHECKED_CAST")
                                ifValueExists(value as T)
                            },
                            ifFalse = { ifValueNotExists() }
                    )
                }
            }
    )

}

fun <T> Lateinit.Companion.async(
        safeContext: SafeContextAsync
) = LateinitAsync<T>(
        safeContext = safeContext
)

fun <T> Lateinit.Companion.mutex() =
        async<T>(SafeContextAsync.mutex())

fun <T> Lateinit.Companion.unsafeAsync() =
        async<T>(SafeContextAsync.unsafe)