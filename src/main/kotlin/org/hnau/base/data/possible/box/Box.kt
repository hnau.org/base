package org.hnau.base.data.possible.box

import org.hnau.base.extensions.boolean.checkBoolean


abstract class Box<T> {

    companion object

    @PublishedApi
    internal var valueExists = false

    @PublishedApi
    internal var value: T? = null

    protected fun setUnsafe(
            valueExists: Boolean,
            value: T?
    ) {
        this.value = value
        this.valueExists = valueExists
    }

    protected inline fun <R> checkValueExistsUnsafe(
            ifExists: (value: T) -> R,
            ifNotExists: () -> R
    ) = valueExists.checkBoolean(
            ifTrue = {
                @Suppress("UNCHECKED_CAST")
                ifExists(value as T)
            },
            ifFalse = ifNotExists
    )


}