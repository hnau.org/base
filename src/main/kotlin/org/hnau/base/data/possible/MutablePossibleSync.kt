package org.hnau.base.data.possible

import org.hnau.base.extensions.it


interface MutablePossibleSync<T> : PossibleSync<T> {

    fun set(value: T)

}

fun <T> MutablePossibleSync<T>.update(
        createInitialValue: () -> T,
        updateValue: T.() -> T
) = checkPossible(
        ifValueExists = updateValue,
        ifValueNotExists = createInitialValue
).also(::set)

inline fun <T> MutablePossibleSync<T>.updateMutable(
        crossinline createInitialValue: () -> T,
        crossinline updateValue: T.() -> Unit
) = checkPossible(
        ifValueExists = { value -> value.also(updateValue) },
        ifValueNotExists = { createInitialValue().also(::set) }
)

inline fun <T> MutablePossibleSync<T>.getOrInitialize(
        crossinline valueToInitializeIfEmpty: () -> T
) = updateMutable(
        createInitialValue = valueToInitializeIfEmpty,
        updateValue = {}
)