package org.hnau.base.data.possible


object PossibleUtils {

    fun throwValueIsNotInitializedException(): Nothing =
            error("Value is not initialized")

}