package org.hnau.base.data.settings


abstract class SettingsSection<P, T>(
        private val pathProvider: SettingsPathProvider<P, T>
) {

    protected fun nextProvider(pathIdentifier: P) =
            pathProvider.next(pathIdentifier)

}