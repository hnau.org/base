package org.hnau.base.data.settings

import org.hnau.base.data.delegate.mutable.MutableDelegate
import org.hnau.base.data.delegate.mutable.create


inline fun <P, T> SettingsConnector.Companion.create(
        crossinline read: (path: Iterable<P>) -> T,
        crossinline write: (path: Iterable<P>, value: T) -> Unit
) = object : SettingsConnector<P, T> {
    override fun read(path: Iterable<P>) = read.invoke(path)
    override fun write(path: Iterable<P>, value: T) = write.invoke(path, value)
}

fun <P, T> SettingsConnector<P, T>.createProperty(
        path: Iterable<P>
) = MutableDelegate.create(
        get = { read(path) },
        set = { value -> write(path, value) }
)