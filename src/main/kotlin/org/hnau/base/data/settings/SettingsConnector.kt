package org.hnau.base.data.settings


interface SettingsConnector<P, T> {

    companion object

    fun read(path: Iterable<P>): T

    fun write(path: Iterable<P>, value: T)

}