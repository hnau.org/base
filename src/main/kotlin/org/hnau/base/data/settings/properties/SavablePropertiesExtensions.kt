package org.hnau.base.data.settings.properties

import org.hnau.base.data.renter.Renter
import org.hnau.base.data.renter.inputStreamRenter
import org.hnau.base.data.renter.outputStreamRenter
import org.hnau.base.data.settings.SettingsConnector
import org.hnau.base.data.settings.create
import java.io.File
import java.io.InputStream
import java.io.OutputStream
import java.util.*


fun <I> SavableProperties.toSettingsConnector(
        buildPath: (Iterable<I>) -> String
) = SettingsConnector.create<I, String?>(
        read = { path ->
            properties.getProperty(buildPath(path))
        },
        write = { path, value ->
            properties.setProperty(buildPath(path), value)
            saveProperties()
        }
)

fun SavableProperties.toSettingsConnector() =
        toSettingsConnector<String> { path ->
            path.joinToString(".")
        }

fun SavableProperties.Companion.fromStreams(
        inputStreamRenter: Renter<InputStream>,
        outputStreamRenter: Renter<OutputStream>
) = object : SavableProperties {

    override val properties by lazy {
        inputStreamRenter { inputStream ->
            Properties().apply { load(inputStream) }
        }
    }

    override fun saveProperties() {
        outputStreamRenter { outputStream ->
            properties.store(outputStream, null)
        }
    }
}

fun SavableProperties.Companion.fromFile(
        file: File
) = fromStreams(
        inputStreamRenter = file.inputStreamRenter,
        outputStreamRenter = file.outputStreamRenter
)