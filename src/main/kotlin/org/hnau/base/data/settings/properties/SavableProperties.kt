package org.hnau.base.data.settings.properties

import java.util.*


interface SavableProperties {

    companion object

    val properties: Properties

    fun saveProperties()

}