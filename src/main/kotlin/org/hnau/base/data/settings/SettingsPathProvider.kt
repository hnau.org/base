package org.hnau.base.data.settings

import org.hnau.base.data.delegate.mutable.MutableDelegate
import org.hnau.base.data.tree.PathProvider


class SettingsPathProvider<P, T>(
        private val connector: SettingsConnector<P, T>
) : PathProvider<P, MutableDelegate<T>>() {

    override fun get(path: Iterable<P>) =
            connector.createProperty(path)

    fun next(
            pathIdentifier: P
    ) = createNextPathBuilder(
            pathIdentifier = pathIdentifier
    ).let { nextPathBuilder ->
        SettingsPathProvider<P, T>(
                connector = SettingsConnector.create(
                        read = { path ->
                            connector.read(nextPathBuilder(path))
                        },
                        write = { path, value ->
                            connector.write(nextPathBuilder(path), value)
                        }
                )
        )
    }

}