package org.hnau.base.data.mapper

import org.hnau.base.data.single.Single


fun <I, O> Mapper.Companion.single(
        mapper: Mapper<I, O>
) = Mapper<Single<I>, Single<O>>(
        direct = { (value) ->
            Single(mapper.direct(value))
        },
        reverse = { (value) ->
            Single(mapper.reverse(value))
        }
)

fun <AI, BI, AO, BO> Mapper.Companion.pair(
        firstMapper: Mapper<AI, AO>,
        secondMapper: Mapper<BI, BO>
) = Mapper<Pair<AI, BI>, Pair<AO, BO>>(
        direct = { (first, second) ->
            firstMapper.direct(first) to secondMapper.direct(second)
        },
        reverse = { (first, second) ->
            firstMapper.reverse(first) to secondMapper.reverse(second)
        }
)

fun <AI, BI, CI, AO, BO, CO> Mapper.Companion.triple(
        firstMapper: Mapper<AI, AO>,
        secondMapper: Mapper<BI, BO>,
        thirdMapper: Mapper<CI, CO>
) = Mapper<Triple<AI, BI, CI>, Triple<AO, BO, CO>>(
        direct = { (first, second, third) ->
            Triple(firstMapper.direct(first), secondMapper.direct(second), thirdMapper.direct(third))
        },
        reverse = { (first, second, third) ->
            Triple(firstMapper.reverse(first), secondMapper.reverse(second), thirdMapper.reverse(third))
        }
)