package org.hnau.base.data.mapper


inline fun <T, reified E : Enum<*>> Mapper.Companion.toEnum(
        crossinline extractValue: E.() -> T
): Mapper<T, E> {
    val classOfE = E::class.java
    return Mapper(
            direct = { value ->
                classOfE.enumConstants
                        .find { enum ->
                            enum.extractValue() == value
                        }
                        ?: error("Unable to find item of $classOfE for '$value'")
            },
            reverse = { enum -> enum.extractValue() }
    )
}

inline fun <reified E : Enum<*>> Mapper.Companion.nameToEnum() =
        toEnum<String, E>(Enum<*>::name)

inline fun <reified E : Enum<*>> Mapper.Companion.ordinalToEnum() =
        toEnum<Int, E>(Enum<*>::ordinal)