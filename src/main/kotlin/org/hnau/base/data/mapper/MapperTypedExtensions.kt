package org.hnau.base.data.mapper

import org.hnau.base.extensions.cast


data class TypedConverter<I, O>(
        val type: Class<I>,
        val convert: (I) -> O
)

@PublishedApi
internal inline fun <reified I, O> ((I) -> O).toTyped() = TypedConverter(
        type = I::class.java,
        convert = this
)


data class TypedReverceMapper<I, O>(
        val direct: (I) -> O,
        val typedReverce: TypedConverter<O, I>
)

inline fun <I, reified O> Mapper<I, O>.toTypedReverce() = TypedReverceMapper(
        direct = direct,
        typedReverce = reverse.toTyped<O, I>()
)

fun <I, K, M, O : Any> Mapper.Companion.typed(
        typesMappers: List<Pair<K, TypedReverceMapper<M, out O>>>,
        sourceToKeyAndMiddeSourceMapper: Mapper<I, Pair<K, M>>
): Mapper<I, O> {
    val keyToDirectMap = typesMappers.associate { (key, mapper) ->
        key to mapper.direct
    }
    val classToKeyWithReverceMap = typesMappers.associate { (key, mapper) ->
        mapper.typedReverce.type to (key to mapper.typedReverce.convert)
    }
    return Mapper<I, O>(
            direct = { source ->
                val (type, middleSource) = sourceToKeyAndMiddeSourceMapper.direct(source)
                val converter = keyToDirectMap.getValue(type)
                converter(middleSource)
            },
            reverse = { backSource ->
                val backSourceClass = backSource::class.java
                val (key, converter) = classToKeyWithReverceMap.getValue(backSourceClass)
                val middleSource = converter.cast<(O) -> M>()(backSource)
                sourceToKeyAndMiddeSourceMapper.reverse(key to middleSource)
            }
    )
}