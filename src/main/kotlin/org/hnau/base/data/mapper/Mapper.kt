package org.hnau.base.data.mapper


data class Mapper<I, O>(
        val direct: (I) -> O,
        val reverse: (O) -> I
) {

    companion object

}