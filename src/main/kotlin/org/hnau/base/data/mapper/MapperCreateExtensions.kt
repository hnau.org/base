package org.hnau.base.data.mapper

import org.hnau.base.extensions.it


fun <T> Mapper.Companion.pipe() = Mapper<T, T>(::it, ::it)