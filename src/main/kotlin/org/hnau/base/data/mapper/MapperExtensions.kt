package org.hnau.base.data.mapper

import org.hnau.base.extensions.doThrow
import org.hnau.base.extensions.it


inline fun <IN_I, IN_O, OUT_I, OUT_O> Mapper<IN_I, IN_O>.map(
        convertDirect: ((IN_I) -> IN_O) -> ((OUT_I) -> OUT_O),
        convertReverse: ((IN_O) -> IN_I) -> ((OUT_O) -> OUT_I)
) = Mapper(
        direct = convertDirect(direct),
        reverse = convertReverse(reverse)
)

val <I, O> Mapper<I, O>.nullable
    get() = map<I, O, I?, O?>(
            convertDirect = { direct -> { i -> i?.let(direct) } },
            convertReverse = { reverse -> { o -> o?.let(reverse) } }
    )

inline fun <T> Mapper.Companion.notNull(
        crossinline ifNull: () -> T = { throw IllegalArgumentException("Null is not supported") }
) = Mapper<T?, T>(
        direct = { value -> value ?: ifNull() },
        reverse = ::it
)

val <I, O> Mapper<I, O>.reversed
    get() = Mapper(reverse, direct)

operator fun <A, B, C> Mapper<A, B>.plus(
        other: Mapper<B, C>
) = Mapper<A, C>(
        direct = { direct(it).let(other.direct) },
        reverse = { reverse(other.reverse(it)) }
)