package org.hnau.base.data.mapper

import org.hnau.base.extensions.elvis
import org.hnau.base.extensions.number.takeIfNotNegative


private val stringToStringsPairByFirstLengthInner = Mapper<String, Pair<String, String>>(
        direct = { string ->
            string.indexOf('_')
                    .takeIfNotNegative()
                    .elvis { error("Unable to define length on first string, there are no '_' in string '$string'") }
                    .let { indexOfSeparator ->
                        val firstStringLength = string
                                .substring(0, indexOfSeparator)
                                .let { firstStringLengthString ->
                                    firstStringLengthString
                                            .toIntOrNull()
                                            .elvis { error("Unable to define length of first string. '$firstStringLengthString' is not a number") }
                                }
                        val firstString = string.substring(indexOfSeparator + 1, indexOfSeparator + 1 + firstStringLength)
                        val secondString = string.substring(indexOfSeparator + 1 + firstStringLength)
                        firstString to secondString
                    }
        },
        reverse = { (first, second) ->
            "${first.length}_$first$second"
        }
)

val Mapper.Companion.stringToStringsPairByFirstLength
    get() = stringToStringsPairByFirstLengthInner

fun Mapper.Companion.stringToStringsPairBySeparator(
        separator: String = ":"
) = Mapper<String, Pair<String, String>>(
        direct = { string ->
            string.indexOf(separator)
                    .takeIfNotNegative()
                    .elvis { error("Unable to find two strings separator '$separator' in string '$string'") }
                    .let { indexOfSeparator ->
                        val firstString = string.substring(0, indexOfSeparator)
                        val secondString = string.substring(indexOfSeparator + 1)
                        firstString to secondString
                    }
        },
        reverse = { (first, second) ->
            "$first$separator$second"
        }
)