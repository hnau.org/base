package org.hnau.base.data.mapper

import org.hnau.base.extensions.checkNullable
import org.hnau.base.extensions.elvis
import org.hnau.base.extensions.number.takeIfNotNegative
import java.nio.charset.Charset


fun Mapper.Companion.bytesToString(
        charset: Charset = Charsets.UTF_8
) = Mapper<ByteArray, String>(
        reverse = { it.toByteArray(charset) },
        direct = { it.toString(charset) }
)

private val bytesToStringInner = Mapper.bytesToString()
val Mapper.Companion.bytesToString: Mapper<ByteArray, String> get() = bytesToStringInner

private val stringToByteInner = Mapper(String::toByte, Byte::toString)
val Mapper.Companion.stringToByte get() = stringToByteInner

private val stringToShortInner = Mapper(String::toShort, Short::toString)
val Mapper.Companion.stringToShort get() = stringToShortInner

private val stringToIntInner = Mapper(String::toInt, Int::toString)
val Mapper.Companion.stringToInt get() = stringToIntInner

private val stringToLongInner = Mapper(String::toLong, Long::toString)
val Mapper.Companion.stringToLong get() = stringToLongInner

private val stringToFloatInner = Mapper(String::toFloat, Float::toString)
val Mapper.Companion.stringToFloat get() = stringToFloatInner

private val stringToDoubleInner = Mapper(String::toDouble, Double::toString)
val Mapper.Companion.stringToDouble get() = stringToDoubleInner

private val stringToBooleanInner = Mapper(String::toBoolean, Boolean::toString)
val Mapper.Companion.stringToBoolean get() = stringToBooleanInner