package org.hnau.base.data.single

import org.hnau.base.data.mapper.Mapper


data class Single<out T>(
        val value: T
) {

    companion object {

        fun <T> valueToSingleMapper() = Mapper(
                ::Single,
                Single<T>::value
        )

    }

}