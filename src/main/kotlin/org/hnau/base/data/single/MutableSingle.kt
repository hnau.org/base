package org.hnau.base.data.single


data class MutableSingle<T>(
        var value: T
)