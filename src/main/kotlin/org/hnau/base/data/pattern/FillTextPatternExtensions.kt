package org.hnau.base.data.pattern

import org.hnau.base.extensions.number.takeIfNotNegative


fun String.asPatternFill(
        values: Map<String, String>
): String {
    var result = this
    values.forEach { (placeholder, value) ->
        val index = result.indexOf(placeholder)
                .takeIfNotNegative()
                ?: error("Placeholder '$placeholder' not found in '$this'")
        result = result.replaceRange(index, index + placeholder.length, value.toString())
        result.indexOf(placeholder)
                .takeIf { it == -1 }
                ?: error("Duplicated placeholder '$placeholder' in '$this'")
    }
    return result
}

fun String.asPatternFill(
        vararg values: Pair<String, String>
) = asPatternFill(
        values = mapOf(*values)
)

fun String.asPatternFill(
        placeholder: String,
        value: String
) = asPatternFill(
        placeholder to value
)