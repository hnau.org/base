package org.hnau.base.data.pattern

import org.hnau.base.extensions.number.takeIfNotNegative


abstract class TextPattern(
        private val unspecifiedText: String
) {

    protected abstract fun getItems(): Iterable<Pair<String, *>>

    val text: String
        get() = unspecifiedText.let { unspecifiedText ->
            var result = unspecifiedText
            getItems().forEach { (placeholder, value) ->
                val index = result.indexOf(placeholder)
                        .takeIfNotNegative()
                        ?: error("Placeholder '$placeholder' not found in '${this.unspecifiedText}'")
                result = result.replaceRange(index, index + placeholder.length, value.toString())
                result.indexOf(placeholder)
                        .takeIf { it == -1 }
                        ?: error("Duplicated placeholder '$placeholder' in '${this.unspecifiedText}'")
            }
            result
        }

}