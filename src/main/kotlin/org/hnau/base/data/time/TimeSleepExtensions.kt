package org.hnau.base.data.time


fun Time.sleep() =
        Thread.sleep(milliseconds)