package org.hnau.base.data.time

import java.util.*


/**
 * Return [Time] initialized by `System.currentTimeMillis()`
 */
fun Time.Companion.now() =
        Time(System.currentTimeMillis())

/**
 * Return [Time] initialized by nanoseconds
 */
fun Time.Companion.fromNanoseconds(nanoseconds: Number) =
        fromMicroseconds(nanoseconds.toLong() / 1000L)

/**
 * Return [Time] initialized by microseconds
 */
fun Time.Companion.fromMicroseconds(microseconds: Number) =
        Time(microseconds.toLong() / 1000L)

/**
 * Return [Time] initialized by [Calendar]
 */

operator fun Time.Companion.invoke(calendar: Calendar) =
        Time(calendar.timeInMillis)

/**
 * Return [Time] initialized by [Date]
 */
operator fun Time.Companion.invoke(date: Date) =
        Time(date.time)