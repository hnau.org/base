package org.hnau.base.data.time

/**
 * Return [Time] initialized as nanoseconds
 */
val Number.asNanoseconds get() = Time.fromNanoseconds(this)

/**
 * Return [Time] initialized as microseconds
 */
val Number.asMicroseconds get() = Time.fromMicroseconds(this)

/**
 * Return [Time] initialized as milliseconds
 */
val Number.asMilliseconds get() = Time.millisecond * this

/**
 * Return [Time] initialized as seconds
 */
val Number.asSeconds get() = Time.second * this

/**
 * Return [Time] initialized as minutes
 */
val Number.asMinutes get() = Time.minute * this

/**
 * Return [Time] initialized as hours
 */
val Number.asHours get() = Time.hour * this

/**
 * Return [Time] initialized as days
 */
val Number.asDays get() = Time.day * this