package org.hnau.base.data.time

import org.hnau.base.extensions.ifEqualsTo
import org.hnau.base.extensions.ifLargeThan
import org.hnau.base.extensions.ifLessThan
import org.hnau.base.extensions.ifNotEqualsTo
import org.hnau.base.extensions.ifNotLargeThan
import org.hnau.base.extensions.ifNotLessThan
import org.hnau.base.extensions.it


inline fun <R> Time.ifPositive(action: (Time) -> R) = ifLargeThan(Time.zero, action)
inline fun <R> Time.ifNegative(action: (Time) -> R) = ifLessThan(Time.zero, action)
inline fun <R> Time.ifNotNegative(action: (Time) -> R) = ifNotLessThan(Time.zero, action)
inline fun <R> Time.ifNotPositive(action: (Time) -> R) = ifNotLargeThan(Time.zero, action)
inline fun <R> Time.ifZero(action: () -> R) = ifEqualsTo(Time.zero, action)
inline fun <R> Time.ifNotZero(action: (Time) -> R) = ifNotEqualsTo(Time.zero, action)

fun Time.takeIfPositive() = ifPositive(::it)
fun Time.takeIfNegative() = ifNegative(::it)
fun Time.takeIfNotNegative() = ifNotNegative(::it)
fun Time.takeIfNotPositive() = ifNotPositive(::it)
fun Time.takeIfZero() = ifZero { this }
fun Time.takeIfNotZero() = ifNotZero(::it)