package org.hnau.base.data.time


/**
 * Return milliseconds without seconds count
 */
val Time.millisecondsDigits: Int
    get() = (milliseconds - seconds * 1000).toInt()

/**
 * Return seconds without minutes count
 */
val Time.secondsDigits: Int
    get() = (seconds - minutes * 60).toInt()

/**
 * Return minutes without hours count
 */
val Time.minutesDigits: Int
    get() = (minutes - hours * 60).toInt()

/**
 * Return hours without days count
 */
val Time.hoursDigits: Int
    get() = (hours - days * 24).toInt()

/**
 * Return days count
 */
val Time.daysDigits: Int
    get() = days.toInt()