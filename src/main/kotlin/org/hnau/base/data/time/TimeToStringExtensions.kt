package org.hnau.base.data.time

import org.hnau.base.extensions.container.takeIfNotEmpty

/**
 * Return [String] with milliseconds count and `ms` suffix
 */
fun Time.toMillisecondsString() = "${milliseconds}ms"

/**
 * Return [String] with seconds count and `s` suffix
 */
fun Time.toSecondsString() = "${seconds}s"

/**
 * Return [String] with minutes count and `m` suffix
 */
fun Time.toMinutesString() = "${minutes}m"

/**
 * Return [String] with hours count and `h` suffix
 */
fun Time.toHoursString() = "${hours}h"

/**
 * Return [String] with days count and `d` suffix
 */
fun Time.toDaysString() = "${days}d"


/**
 * Return [String] with formatted to days, hours, minutes, seconds and milliseconds values
 * @param levels levels count
 */
fun Time.toLevelsString(levels: Int = 3) =
        listOf(
                daysDigits to "d",
                hoursDigits to "h",
                minutesDigits to "m",
                secondsDigits to "s",
                millisecondsDigits to "ms"
        )
                .dropWhile { it.first <= 0 }
                .take(levels)
                .takeIfNotEmpty()
                ?.joinToString(
                        separator = "",
                        transform = { it.first.toString() + it.second }
                ) ?: "0ms"