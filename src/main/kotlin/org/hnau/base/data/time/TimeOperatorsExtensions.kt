package org.hnau.base.data.time


operator fun Number.times(time: Time) =
        Time((toDouble() * time.milliseconds).toLong())