package org.hnau.base.data.time

import org.hnau.base.data.mapper.Mapper


private val longToTimeMapper =
        Mapper(Number::asMilliseconds, Time::milliseconds)

val Time.Companion.longMapper
    get() = longToTimeMapper