package org.hnau.base.data.time

/**
 * Return seconds count
 */
val Time.seconds: Long
    get() = milliseconds / 1000

/**
 * Return minutes count
 */
val Time.minutes: Long
    get() = seconds / 60

/**
 * Return hours count
 */
val Time.hours: Long
    get() = minutes / 60

/**
 * Return days count
 */
val Time.days: Long
    get() = hours / 24