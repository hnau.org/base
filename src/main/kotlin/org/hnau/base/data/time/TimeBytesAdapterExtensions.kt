package org.hnau.base.data.time

import org.hnau.base.data.bytes.adapter.BytesAdapter
import org.hnau.base.data.bytes.adapter.long
import org.hnau.base.data.bytes.adapter.map


private val timeBytesAdapter =
        BytesAdapter.long.map(Time.longMapper)

val Time.Companion.bytesAdapter
    get() = timeBytesAdapter