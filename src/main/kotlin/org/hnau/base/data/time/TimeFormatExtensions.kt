package org.hnau.base.data.time

import org.hnau.base.utils.TimeZoneUtils
import java.text.SimpleDateFormat


fun Time.Companion.formatter(
        format: String = "yyyy-MM-dd_HH:mm:ss.S"
): (Time) -> String = SimpleDateFormat(format)
        .apply { timeZone = TimeZoneUtils.UTC }
        .run { { time -> format(time.milliseconds) } }