package org.hnau.base.data.time

import java.util.*

/**
 * Return [Calendar] initialized by [this]
 */
fun Time.toCalendar(): Calendar =
        Calendar.getInstance().apply { timeInMillis = milliseconds }

/**
 * Return [Date] initialized by [this]
 */
fun Time.toDate() =
        Date(milliseconds)