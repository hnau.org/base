package org.hnau.base.extensions

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.CoroutineStart
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withTimeout
import kotlinx.coroutines.withTimeoutOrNull
import org.hnau.base.data.time.Time
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext


fun CoroutineScope.toLauncher(
        context: CoroutineContext = EmptyCoroutineContext,
        start: CoroutineStart = CoroutineStart.DEFAULT
): (suspend CoroutineScope.() -> Unit) -> Job = { block ->
    launch(
            context = context,
            start = start,
            block = block
    )
}

fun <T> CoroutineScope.toAsyncExecutor(
        context: CoroutineContext = EmptyCoroutineContext,
        start: CoroutineStart = CoroutineStart.DEFAULT
): (suspend CoroutineScope.() -> T) -> Deferred<T> = { block ->
    async(
            context = context,
            start = start,
            block = block
    )
}

/**
 * Delays coroutine for a given time without blocking a thread and resumes it after a specified [time].
 */
suspend fun delay(time: Time) =
        delay(time.milliseconds)

/**
 * Runs a given suspending [block] of code inside a coroutine with a specified timeout and throws
 */
suspend fun <T> withTimeout(time: Time, block: suspend CoroutineScope.() -> T) =
        withTimeout(time.milliseconds, block)

/**
 * Runs a given suspending [block] of code inside a coroutine with a specified timeout and returns
 * `null` if this timeout was exceeded.
 */
suspend fun <T : Any> withTimeoutOrNull(time: Time, block: suspend CoroutineScope.() -> T) =
        withTimeoutOrNull(time.milliseconds, block)