package org.hnau.base.extensions.container

import org.hnau.base.extensions.doThrow


inline fun <I, O> Triple<I, I, I>.map(converter: (I) -> O) =
        Triple(converter(first), converter(second), converter(third))

inline fun <T> Triple<T, T, T>.forEach(action: (T) -> Unit) {
    action(first)
    action(second)
    action(third)
}

fun <T> Triple<T, T, T>.toList() =
        listOf(first, second, third)

@Suppress("UNCHECKED_CAST")
inline fun <T> Iterable<T>.toTriple(
        onLessThan3Elements: (first: T?, second: T?) -> Triple<T, T, T> =
                { _, _ -> throw IllegalArgumentException("There are less than 3 elements in Iterable") }
): Triple<T, T, T> {
    var first: T? = null
    var second: T? = null
    forEachIndexed { index, item ->
        when (index) {
            0 -> first = item
            1 -> second = item
            else -> Triple(first as T, second as T, item)
        }
    }
    return onLessThan3Elements(first, second)
}

fun <A, B, C> Triple<A, B, C>.reverse() =
        Triple(third, second, first)

val <A, B, C> Triple<A, B, C>.pair12: Pair<A, B>
    get() = Pair(first, second)

val <A, B, C> Triple<A, B, C>.pair13: Pair<A, C>
    get() = Pair(first, third)

val <A, B, C> Triple<A, B, C>.pair23: Pair<B, C>
    get() = Pair(second, third)