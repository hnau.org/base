package org.hnau.base.extensions.container

import org.hnau.base.extensions.checkNullable
import org.hnau.base.extensions.doIf
import org.hnau.base.extensions.it
import org.hnau.base.extensions.letIf


inline fun <T, R> Array<T>.ifEmpty(action: () -> R) = doIf(Array<T>::isEmpty, action)
inline fun <T, R> Array<T>.ifNotEmpty(action: (Array<T>) -> R) = letIf(Array<T>::isNotEmpty, action)
fun <T> Array<T>.takeIfEmpty() = ifEmpty { this }
fun <T> Array<T>.takeIfNotEmpty() = ifNotEmpty(::it)
inline fun <T, R> Array<T>.checkIsNotEmpty(ifNotEmpty: (Array<T>) -> R, ifEmpty: () -> R) = 
        takeIfNotEmpty().checkNullable(ifNotNull = ifNotEmpty, ifNull = ifEmpty)

inline fun <R> ByteArray.ifEmpty(action: () -> R) = doIf(ByteArray::isEmpty, action)
inline fun <R> ByteArray.ifNotEmpty(action: (ByteArray) -> R) = letIf(ByteArray::isNotEmpty, action)
fun ByteArray.takeIfEmpty() = ifEmpty { this }
fun ByteArray.takeIfNotEmpty() = ifNotEmpty(::it)
inline fun <R> ByteArray.checkIsNotEmpty(ifNotEmpty: (ByteArray) -> R, ifEmpty: () -> R) =
        takeIfNotEmpty().checkNullable(ifNotNull = ifNotEmpty, ifNull = ifEmpty)

inline fun <R> ShortArray.ifEmpty(action: () -> R) = doIf(ShortArray::isEmpty, action)
inline fun <R> ShortArray.ifNotEmpty(action: (ShortArray) -> R) = letIf(ShortArray::isNotEmpty, action)
fun ShortArray.takeIfEmpty() = ifEmpty { this }
fun ShortArray.takeIfNotEmpty() = ifNotEmpty(::it)
inline fun <R> ShortArray.checkIsNotEmpty(ifNotEmpty: (ShortArray) -> R, ifEmpty: () -> R) =
        takeIfNotEmpty().checkNullable(ifNotNull = ifNotEmpty, ifNull = ifEmpty)

inline fun <R> IntArray.ifEmpty(action: () -> R) = doIf(IntArray::isEmpty, action)
inline fun <R> IntArray.ifNotEmpty(action: (IntArray) -> R) = letIf(IntArray::isNotEmpty, action)
fun IntArray.takeIfEmpty() = ifEmpty { this }
fun IntArray.takeIfNotEmpty() = ifNotEmpty(::it)
inline fun <R> IntArray.checkIsNotEmpty(ifNotEmpty: (IntArray) -> R, ifEmpty: () -> R) =
        takeIfNotEmpty().checkNullable(ifNotNull = ifNotEmpty, ifNull = ifEmpty)

inline fun <R> LongArray.ifEmpty(action: () -> R) = doIf(LongArray::isEmpty, action)
inline fun <R> LongArray.ifNotEmpty(action: (LongArray) -> R) = letIf(LongArray::isNotEmpty, action)
fun LongArray.takeIfEmpty() = ifEmpty { this }
fun LongArray.takeIfNotEmpty() = ifNotEmpty(::it)
inline fun <R> LongArray.checkIsNotEmpty(ifNotEmpty: (LongArray) -> R, ifEmpty: () -> R) =
        takeIfNotEmpty().checkNullable(ifNotNull = ifNotEmpty, ifNull = ifEmpty)

inline fun <R> FloatArray.ifEmpty(action: () -> R) = doIf(FloatArray::isEmpty, action)
inline fun <R> FloatArray.ifNotEmpty(action: (FloatArray) -> R) = letIf(FloatArray::isNotEmpty, action)
fun FloatArray.takeIfEmpty() = ifEmpty { this }
fun FloatArray.takeIfNotEmpty() = ifNotEmpty(::it)
inline fun <R> FloatArray.checkIsNotEmpty(ifNotEmpty: (FloatArray) -> R, ifEmpty: () -> R) =
        takeIfNotEmpty().checkNullable(ifNotNull = ifNotEmpty, ifNull = ifEmpty)

inline fun <R> DoubleArray.ifEmpty(action: () -> R) = doIf(DoubleArray::isEmpty, action)
inline fun <R> DoubleArray.ifNotEmpty(action: (DoubleArray) -> R) = letIf(DoubleArray::isNotEmpty, action)
fun DoubleArray.takeIfEmpty() = ifEmpty { this }
fun DoubleArray.takeIfNotEmpty() = ifNotEmpty(::it)
inline fun <R> DoubleArray.checkIsNotEmpty(ifNotEmpty: (DoubleArray) -> R, ifEmpty: () -> R) =
        takeIfNotEmpty().checkNullable(ifNotNull = ifNotEmpty, ifNull = ifEmpty)

inline fun <R> CharArray.ifEmpty(action: () -> R) = doIf(CharArray::isEmpty, action)
inline fun <R> CharArray.ifNotEmpty(action: (CharArray) -> R) = letIf(CharArray::isNotEmpty, action)
fun CharArray.takeIfEmpty() = ifEmpty { this }
fun CharArray.takeIfNotEmpty() = ifNotEmpty(::it)
inline fun <R> CharArray.checkIsNotEmpty(ifNotEmpty: (CharArray) -> R, ifEmpty: () -> R) =
        takeIfNotEmpty().checkNullable(ifNotNull = ifNotEmpty, ifNull = ifEmpty)

inline fun <R> BooleanArray.ifEmpty(action: () -> R) = doIf(BooleanArray::isEmpty, action)
inline fun <R> BooleanArray.ifNotEmpty(action: (BooleanArray) -> R) = letIf(BooleanArray::isNotEmpty, action)
fun BooleanArray.takeIfEmpty() = ifEmpty { this }
fun BooleanArray.takeIfNotEmpty() = ifNotEmpty(::it)
inline fun <R> BooleanArray.checkIsNotEmpty(ifNotEmpty: (BooleanArray) -> R, ifEmpty: () -> R) =
        takeIfNotEmpty().checkNullable(ifNotNull = ifNotEmpty, ifNull = ifEmpty)