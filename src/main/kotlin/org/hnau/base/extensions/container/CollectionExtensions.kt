package org.hnau.base.extensions.container

import org.hnau.base.extensions.checkNullable
import org.hnau.base.extensions.doIf
import org.hnau.base.extensions.it
import org.hnau.base.extensions.letIf


inline fun <T, C : Collection<T>, R> C.ifEmpty(action: () -> R) = doIf(Collection<T>::isEmpty, action)
inline fun <T, C : Collection<T>, R> C.ifNotEmpty(action: (C) -> R) = letIf(Collection<T>::isNotEmpty, action)
fun <T, C : Collection<T>> C.takeIfEmpty() = ifEmpty { this }
fun <T, C : Collection<T>> C.takeIfNotEmpty() = ifNotEmpty(::it)
inline fun <T, C : Collection<T>, R> C.checkIsNotEmpty(ifNotEmpty: (C) -> R, ifEmpty: () -> R) =
        takeIfNotEmpty().checkNullable(ifNotNull = ifNotEmpty, ifNull = ifEmpty)