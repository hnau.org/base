package org.hnau.base.extensions.container

import org.hnau.base.extensions.boolean.checkBoolean


inline fun <T> Iterable<T>.mapIf(
        predicate: (T) -> Boolean,
        transform: (T) -> T
) = map { item ->
    predicate(item).checkBoolean(
            ifFalse = { item },
            ifTrue = { transform(item) }
    )
}