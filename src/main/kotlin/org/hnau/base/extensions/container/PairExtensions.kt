package org.hnau.base.extensions.container

import org.hnau.base.extensions.doThrow


inline fun <I, O> Pair<I, I>.map(converter: (I) -> O) =
        Pair(converter.invoke(first), converter.invoke(second))

inline fun <T> Pair<T, T>.forEach(action: (T) -> Unit) {
    action.invoke(first)
    action.invoke(second)
}

fun <T> Pair<T, T>.toList() =
        listOf(first, second)

@Suppress("UNCHECKED_CAST")
inline fun <T> Iterable<T>.toPair(
        onLessThan2Elements: (first: T?) -> Pair<T, T> =
                { throw IllegalArgumentException("There are less than 2 elements in Iterable") }
): Pair<T, T> {
    var first: T? = null
    forEachIndexed { index, item ->
        if (index > 0) {
            return Pair(first as T, item)
        } else {
            first = item
        }
    }
    return onLessThan2Elements(first)
}


fun <A, B> Pair<A, B>.reverse() =
        Pair(second, first)

infix fun <A, B, C> Pair<A, B>.join(third: C) =
        Triple(first, second, third)

infix fun <A, B, C> A.join(pair: Pair<B, C>) =
        Triple(this, pair.first, pair.second)

infix fun <A, B, C> Pair<A, C>.insert(value: B) =
        Triple(first, value, second)

infix fun <A, B, C> B.insert(pair: Pair<A, C>) =
        Triple(pair.first, this, pair.second)