package org.hnau.base.extensions.container


inline fun <T> MutableIterable<T>.removeFirst(
        predicate: (T) -> Boolean
): T? {
    with(iterator()) {
        while (hasNext()) {
            val value = next()
            if (predicate(value)) {
                remove()
                return value
            }
        }
    }
    return null
}