package org.hnau.base.extensions

import org.hnau.base.extensions.boolean.ifTrue
import kotlin.reflect.KMutableProperty0


inline fun <T> setUnique(oldValue: T, value: T, set: (T) -> Unit) =
        (oldValue != value).apply { ifTrue { set(value) } }

fun <T> KMutableProperty0<T>.setUnique(value: T) =
        setUnique(get(), value, this::set)

inline fun <T> T.updateUnique(value: T, set: (T) -> Unit) =
        setUnique(this, value, set)