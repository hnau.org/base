package org.hnau.base.extensions

/**
 * Return result of executing [action] if empty, or `null`
 */
inline fun <R> String.ifEmpty(action: () -> R) = doIf(String::isEmpty, action)

/**
 * Return result of executing [action] if not empty, or `null`
 */
inline fun <R> String.ifNotEmpty(action: (String) -> R) = letIf(String::isNotEmpty, action)

/**
 * Return result of executing [action] if blank, or `null`
 */
inline fun <R> String.ifBlank(action: () -> R) = doIf(String::isBlank, action)

/**
 * Return result of executing [action] if not blank, or `null`
 */
inline fun <R> String.ifNotBlank(action: (String) -> R) = letIf(String::isNotBlank, action)

/**
 * Return [this] if empty, or `null`
 */
fun String.takeIfEmpty() = ifEmpty { this }

/**
 * Return [this] if not empty, or `null`
 */
fun String.takeIfNotEmpty() = ifNotEmpty(::it)

/**
 * Return [this] if blank, or `null`
 */
fun String.takeIfBlank() = ifBlank { this }

/**
 * Return [this] if not blank, or `null`
 */
fun String.takeIfNotBlank() = ifNotBlank(::it)