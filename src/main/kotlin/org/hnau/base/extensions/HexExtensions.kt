package org.hnau.base.extensions

import org.hnau.base.data.mapper.Mapper
import java.lang.IllegalArgumentException
import kotlin.experimental.and

private val hexChars = "0123456789abcdef".toHashSet()

fun String.asHexToBytes(): ByteArray {

    fun Char.toHex() = toLowerCase()
            .takeIf { char -> char in hexChars }
            .elvis { throw IllegalArgumentException("'$this' is not correct Hex char") }
            .let { char -> Character.digit(char, 16) }

    val length = this.length
    check(length % 2 == 0) { "Hex string length must be even" }
    return ByteArray(length / 2) { byteNumber ->
        val first = get(byteNumber * 2).toHex()
        val second = get(byteNumber * 2 + 1).toHex()
        ((first shl 4) + second).toByte()
    }
}

fun ByteArray.toHexString() = joinToString("") { byte ->
    Integer
            .toUnsignedString(java.lang.Byte.toUnsignedInt(byte), 16)
            .padStart(2, '0')
}

private val hexStringToBytesMapper = Mapper<String, ByteArray>(
        direct = String::asHexToBytes,
        reverse = ByteArray::toHexString
)

val Mapper.Companion.hexStringToBytes
    get() = hexStringToBytesMapper