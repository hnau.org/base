package org.hnau.base.extensions

/**
 * Return [this]
 */
fun <T> T.me() = this

/**
 * Return [from]
 */
fun <T> it(from: T) = from

fun <T> T.useIfNull(nullable: T?) = nullable ?: this