package org.hnau.base.extensions

import java.io.PrintWriter
import java.io.StringWriter

/**
 * Return stack trace as String
 */
val Throwable.stringStackTrace: String
    get() = StringWriter().also { printStackTrace(PrintWriter(it)) }.toString()

val Throwable.messageWithStackTrace: String
    get() = "$message\n$stringStackTrace"

/**
 * Return casted to Exception
 * or Exception with cause
 */
val Throwable.asException: Exception
    get() = this as? Exception ?: Exception(this)

/**
 * Return `null` if is `null`
 * or casted to Exception
 * or Exception with cause
 */
val Throwable?.asExceptionOrNull: Exception?
    get() = this?.asException