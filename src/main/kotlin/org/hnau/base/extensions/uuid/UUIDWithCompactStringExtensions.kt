package org.hnau.base.extensions.uuid

import org.hnau.base.data.bytes.provider.BufferedBytesProvider
import org.hnau.base.data.bytes.receiver.BufferedBytesReceiver
import org.hnau.base.data.mapper.Mapper
import org.hnau.base.data.mapper.plus
import org.hnau.base.extensions.hexStringToBytes
import java.util.*

object UUIDCompactStringUtils {

    const val compactStringLength = 32

}

private val compactStringToUUIDMapper =
        Mapper(String::toUpperCase, String::toUpperCase) + Mapper.hexStringToBytes + Mapper.bytesToUUID

val Mapper.Companion.compactStringToUUID
    get() = compactStringToUUIDMapper

fun UUID.toCompactString() =
        Mapper.compactStringToUUID.reverse(this)

fun String.asCompactStringToUUID() =
        Mapper.compactStringToUUID.direct(this)