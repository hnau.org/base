package org.hnau.base.extensions.uuid

import org.hnau.base.data.bytes.provider.BufferedBytesProvider
import org.hnau.base.data.bytes.receiver.BufferedBytesReceiver
import org.hnau.base.data.mapper.Mapper
import java.util.*


fun UUID.toBytes() = BufferedBytesReceiver()
        .apply { writeUUID(this@toBytes) }
        .buffer

fun ByteArray.toUUID() =
        BufferedBytesProvider(this).readUUID()

private val bytesToUUIDMapper =
        Mapper(ByteArray::toUUID, UUID::toBytes)

val Mapper.Companion.bytesToUUID
    get() = bytesToUUIDMapper