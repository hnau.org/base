package org.hnau.base.extensions.uuid

import org.hnau.base.data.bytes.provider.BufferedBytesProvider
import org.hnau.base.data.bytes.receiver.BufferedBytesReceiver
import org.hnau.base.data.mapper.Mapper
import java.util.*

object UUIDStringUtils {

    const val stringLength = 36

}

private val stringToUUIDMapper = Mapper<String, UUID>(
        direct = { string -> UUID.fromString(string) },
        reverse = UUID::toString
)

val Mapper.Companion.stringToUUID
    get() = stringToUUIDMapper