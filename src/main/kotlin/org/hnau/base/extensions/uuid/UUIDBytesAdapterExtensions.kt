package org.hnau.base.extensions.uuid

import org.hnau.base.data.bytes.adapter.BytesAdapter
import org.hnau.base.data.bytes.provider.BytesProvider
import org.hnau.base.data.bytes.provider.readLong
import org.hnau.base.data.bytes.receiver.BytesReceiver
import org.hnau.base.data.bytes.receiver.writeLong
import java.util.*


fun BytesReceiver.writeUUID(uuid: UUID) {
    writeLong(uuid.mostSignificantBits)
    writeLong(uuid.leastSignificantBits)
}

fun BytesProvider.readUUID() =
        UUID(readLong(), readLong())

private val uuidBytesAdapter = BytesAdapter(
        read = BytesProvider::readUUID,
        write = BytesReceiver::writeUUID
)

val BytesAdapter.Companion.uuid
    get() = uuidBytesAdapter