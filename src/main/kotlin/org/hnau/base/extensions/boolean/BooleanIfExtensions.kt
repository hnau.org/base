package org.hnau.base.extensions.boolean


inline fun <R> Boolean.checkBoolean(ifTrue: () -> R, ifFalse: () -> R) =
        if (this) ifTrue() else ifFalse()