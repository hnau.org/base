package org.hnau.base.extensions.boolean


fun Int.toBoolean() = this != 0
fun Long.toBoolean() = this != 0L
fun Short.toBoolean() = toInt().toBoolean()
fun Byte.toBoolean() = toInt().toBoolean()
fun Float.toBoolean() = this != 0f
fun Double.toBoolean() = this != 0.0