package org.hnau.base.extensions.boolean

import org.hnau.base.extensions.doIf
import org.hnau.base.extensions.it


inline fun <R> Boolean.ifTrue(action: () -> R) = doIf(::it, action)
inline fun <R> Boolean.ifFalse(action: () -> R) = doIf({ !it }, action)

fun Boolean.takeIfTrue() = ifTrue { this }
fun Boolean.takeIfFalse() = ifFalse { this }