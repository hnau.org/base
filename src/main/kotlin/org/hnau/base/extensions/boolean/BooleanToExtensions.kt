package org.hnau.base.extensions.boolean


fun Boolean.toLong() = if (this) 1L else 0L
fun Boolean.toInt() = if (this) 1 else 0
fun Boolean.toByte() = toInt().toByte()
fun Boolean.toShort() = toInt().toShort()
fun Boolean.toFloat() = toInt().toFloat()
fun Boolean.toDouble() = toInt().toDouble()