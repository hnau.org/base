package org.hnau.base.extensions.boolean


fun or(vararg booleans: () -> Boolean) = booleans.any { it() }

fun and(vararg booleans: () -> Boolean) = !booleans.any { !it() }