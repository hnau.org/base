package org.hnau.base.extensions.number

import org.hnau.base.extensions.ifLargeThan
import org.hnau.base.extensions.ifLessThan
import org.hnau.base.extensions.ifNotLargeThan
import org.hnau.base.extensions.ifNotLessThan
import org.hnau.base.extensions.it

fun Float.asPercentageInter(
        from: Number, to: Number
) = from.toFloat().let { fromFloat ->
    fromFloat + (to.toFloat() - fromFloat) * this
}

inline fun <R> Float.ifPositive(action: (Float) -> R) = ifLargeThan(0f, action)
inline fun <R> Float.ifNegative(action: (Float) -> R) = ifLessThan(0f, action)
inline fun <R> Float.ifNotNegative(action: (Float) -> R) = ifNotLessThan(0f, action)
inline fun <R> Float.ifNotPositive(action: (Float) -> R) = ifNotLargeThan(0f, action)

fun Float.takeIfPositive() = ifPositive(::it)
fun Float.takeIfNegative() = ifNegative(::it)
fun Float.takeIfNotNegative() = ifNotNegative(::it)
fun Float.takeIfNotPositive() = ifNotPositive(::it)