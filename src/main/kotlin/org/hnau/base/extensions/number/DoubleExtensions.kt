package org.hnau.base.extensions.number

import org.hnau.base.extensions.ifLargeThan
import org.hnau.base.extensions.ifLessThan
import org.hnau.base.extensions.ifNotLargeThan
import org.hnau.base.extensions.ifNotLessThan
import org.hnau.base.extensions.it

fun Double.asPercentageInter(
        from: Number, to: Number
) = from.toDouble().let { fromDouble ->
    fromDouble + (to.toDouble() - fromDouble) * this
}

inline fun <R> Double.ifPositive(action: (Double) -> R) = ifLargeThan(0.0, action)
inline fun <R> Double.ifNegative(action: (Double) -> R) = ifLessThan(0.0, action)
inline fun <R> Double.ifNotNegative(action: (Double) -> R) = ifNotLessThan(0.0, action)
inline fun <R> Double.ifNotPositive(action: (Double) -> R) = ifNotLargeThan(0.0, action)

fun Double.takeIfPositive() = ifPositive(::it)
fun Double.takeIfNegative() = ifNegative(::it)
fun Double.takeIfNotNegative() = ifNotNegative(::it)
fun Double.takeIfNotPositive() = ifNotPositive(::it)