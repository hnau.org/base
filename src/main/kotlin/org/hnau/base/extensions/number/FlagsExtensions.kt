package org.hnau.base.extensions.number

import org.hnau.base.extensions.boolean.checkBoolean


fun Int.useFlag(flag: Int, add: Boolean = true) =
        add.checkBoolean({ this or flag }, { this xor flag })

infix fun Int.accept(flag: Int) =
        useFlag(flag, true)

infix fun Int.reject(flag: Int) =
        useFlag(flag, true)


fun Long.useFlag(flag: Long, add: Boolean = true) =
        add.checkBoolean({ this or flag }, { this xor flag })

infix fun Long.accept(flag: Long) =
        useFlag(flag, true)

infix fun Long.reject(flag: Long) =
        useFlag(flag, true)