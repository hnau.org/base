package org.hnau.base.extensions.number

import org.hnau.base.extensions.ifEqualsTo
import org.hnau.base.extensions.ifLargeThan
import org.hnau.base.extensions.ifLessThan
import org.hnau.base.extensions.ifNotEqualsTo
import org.hnau.base.extensions.ifNotLargeThan
import org.hnau.base.extensions.ifNotLessThan
import org.hnau.base.extensions.it

inline fun <R> Byte.ifPositive(action: (Byte) -> R) = ifLargeThan(0.toByte(), action)
inline fun <R> Byte.ifNegative(action: (Byte) -> R) = ifLessThan(0.toByte(), action)
inline fun <R> Byte.ifNotNegative(action: (Byte) -> R) = ifNotLessThan(0.toByte(), action)
inline fun <R> Byte.ifNotPositive(action: (Byte) -> R) = ifNotLargeThan(0.toByte(), action)
inline fun <R> Byte.ifZero(action: () -> R) = ifEqualsTo(0.toByte(), action)
inline fun <R> Byte.ifNotZero(action: (Byte) -> R) = ifNotEqualsTo(0.toByte(), action)

fun Byte.takeIfPositive() = ifPositive(::it)
fun Byte.takeIfNegative() = ifNegative(::it)
fun Byte.takeIfNotNegative() = ifNotNegative(::it)
fun Byte.takeIfNotPositive() = ifNotPositive(::it)
fun Byte.takeIfZero() = ifZero { this }
fun Byte.takeIfNotZero() = ifNotZero(::it)