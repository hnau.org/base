package org.hnau.base.extensions.number

import org.hnau.base.extensions.ifEqualsTo
import org.hnau.base.extensions.ifLargeThan
import org.hnau.base.extensions.ifLessThan
import org.hnau.base.extensions.ifNotEqualsTo
import org.hnau.base.extensions.ifNotLargeThan
import org.hnau.base.extensions.ifNotLessThan
import org.hnau.base.extensions.it


inline fun <R> Int.ifPositive(action: (Int) -> R) = ifLargeThan(0, action)
inline fun <R> Int.ifNegative(action: (Int) -> R) = ifLessThan(0, action)
inline fun <R> Int.ifNotNegative(action: (Int) -> R) = ifNotLessThan(0, action)
inline fun <R> Int.ifNotPositive(action: (Int) -> R) = ifNotLargeThan(0, action)
inline fun <R> Int.ifZero(action: () -> R) = ifEqualsTo(0, action)
inline fun <R> Int.ifNotZero(action: (Int) -> R) = ifNotEqualsTo(0, action)

fun Int.takeIfPositive() = ifPositive(::it)
fun Int.takeIfNegative() = ifNegative(::it)
fun Int.takeIfNotNegative() = ifNotNegative(::it)
fun Int.takeIfNotPositive() = ifNotPositive(::it)
fun Int.takeIfZero() = ifZero { this }
fun Int.takeIfNotZero() = ifNotZero(::it)