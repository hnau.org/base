package org.hnau.base.extensions.number

import org.hnau.base.extensions.ifEqualsTo
import org.hnau.base.extensions.ifLargeThan
import org.hnau.base.extensions.ifLessThan
import org.hnau.base.extensions.ifNotEqualsTo
import org.hnau.base.extensions.ifNotLargeThan
import org.hnau.base.extensions.ifNotLessThan
import org.hnau.base.extensions.it

inline fun <R> Short.ifPositive(action: (Short) -> R) = ifLargeThan(0.toShort(), action)
inline fun <R> Short.ifNegative(action: (Short) -> R) = ifLessThan(0.toShort(), action)
inline fun <R> Short.ifNotNegative(action: (Short) -> R) = ifNotLessThan(0.toShort(), action)
inline fun <R> Short.ifNotPositive(action: (Short) -> R) = ifNotLargeThan(0.toShort(), action)
inline fun <R> Short.ifZero(action: () -> R) = ifEqualsTo(0.toShort(), action)
inline fun <R> Short.ifNotZero(action: (Short) -> R) = ifNotEqualsTo(0.toShort(), action)

fun Short.takeIfPositive() = ifPositive(::it)
fun Short.takeIfNegative() = ifNegative(::it)
fun Short.takeIfNotNegative() = ifNotNegative(::it)
fun Short.takeIfNotPositive() = ifNotPositive(::it)
fun Short.takeIfZero() = ifZero { this }
fun Short.takeIfNotZero() = ifNotZero(::it)