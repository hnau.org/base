package org.hnau.base.extensions.number

import org.hnau.base.extensions.ifEqualsTo
import org.hnau.base.extensions.ifLargeThan
import org.hnau.base.extensions.ifLessThan
import org.hnau.base.extensions.ifNotEqualsTo
import org.hnau.base.extensions.ifNotLargeThan
import org.hnau.base.extensions.ifNotLessThan
import org.hnau.base.extensions.it


inline fun <R> Long.ifPositive(action: (Long) -> R) = ifLargeThan(0.toLong(), action)
inline fun <R> Long.ifNegative(action: (Long) -> R) = ifLessThan(0.toLong(), action)
inline fun <R> Long.ifNotNegative(action: (Long) -> R) = ifNotLessThan(0.toLong(), action)
inline fun <R> Long.ifNotPositive(action: (Long) -> R) = ifNotLargeThan(0.toLong(), action)
inline fun <R> Long.ifZero(action: () -> R) = ifEqualsTo(0.toLong(), action)
inline fun <R> Long.ifNotZero(action: (Long) -> R) = ifNotEqualsTo(0.toLong(), action)

fun Long.takeIfPositive() = ifPositive(::it)
fun Long.takeIfNegative() = ifNegative(::it)
fun Long.takeIfNotNegative() = ifNotNegative(::it)
fun Long.takeIfNotPositive() = ifNotPositive(::it)
fun Long.takeIfZero() = ifZero { this }
fun Long.takeIfNotZero() = ifNotZero(::it)