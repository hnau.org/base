package org.hnau.base.extensions


fun Exception.doThrow(): Nothing = throw this

@Suppress("UNUSED_PARAMETER")
fun <P> Exception.doThrow(param: P): Nothing = doThrow()