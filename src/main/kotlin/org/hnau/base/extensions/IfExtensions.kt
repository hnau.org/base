package org.hnau.base.extensions

/**
 * Return [this] if [sign] is `true`, or null
 */
fun <T> T.takeIf(sign: Boolean) = takeIf { sign }

/**
 * Return result of executing [action], if predicate for [this] is `true`, or null
 */
inline fun <T, R> T.letIf(predicate: (T) -> Boolean, action: (T) -> R) =
        takeIf(predicate)?.let(action)

/**
 * Return result of executing [action], if predicate for [this] is `true`, or null
 */
inline fun <T, R> T.runIf(predicate: (T) -> Boolean, action: T.() -> R) =
        takeIf(predicate)?.run(action)

/**
 * Return result of executing [action], if predicate for [this] is `true`, or null
 */
inline fun <T, R> T.doIf(predicate: (T) -> Boolean, action: () -> R) =
        letIf(predicate) { action() }

/**
 * Return result of executing [action], if [this] is equals to [other], or null
 */
inline fun <T, R> T.ifEqualsTo(other: T, action: () -> R) =
        doIf({ it == other }, action)

/**
 * Return result of executing [action], if [this] is not equals to [other], or null
 */
inline fun <T, R> T.ifNotEqualsTo(other: T, action: (T) -> R) =
        letIf({ it != other }, action)

/**
 * Return result of executing [action], if [this] is [other], or null
 */
inline fun <T, R> T.ifEqualsByReferenceTo(other: T, action: () -> R) =
        doIf({ it === other }, action)

/**
 * Return result of executing [action], if [this] is not [other], or null
 */
inline fun <T, R> T.ifNotEqualsByReferenceTo(other: T, action: (T) -> R) =
        letIf({ it !== other }, action)