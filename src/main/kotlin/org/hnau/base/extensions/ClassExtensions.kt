package org.hnau.base.extensions

import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type


val Class<*>.genericTypes: Array<Type>
    get() = genericSuperclass
            .cast<ParameterizedType>()
            .actualTypeArguments

fun <R> Class<*>.getGenericClass(index: Int) =
        genericTypes[index].cast<Class<R>>()