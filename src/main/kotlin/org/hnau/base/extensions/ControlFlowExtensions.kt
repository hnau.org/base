package org.hnau.base.extensions


fun pass() {}

@Suppress("UNUSED_PARAMETER")
fun <P> pass(param: P) = pass()

inline fun whileTrue(
        condition: () -> Boolean
) {
    @Suppress("ControlFlowWithEmptyBody")
    while (condition()) {
    }
}