package org.hnau.base.extensions


@Suppress("NOTHING_TO_INLINE")
inline fun stackHead(): StackTraceElement = Exception().stackTrace.first()

