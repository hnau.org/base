package org.hnau.base.extensions


/**
 * Return result of executing [ifNotNull] if not null, or result of executing [ifNull]
 */
inline fun <T, R> T?.checkNullable(ifNotNull: (T) -> R, ifNull: () -> R) =
        if (this != null) ifNotNull(this) else ifNull()

/**
 * Return result of executing [ifNull] if null or return null
 */
inline fun <T, R> T.ifNull(ifNull: () -> R) =
        checkNullable({ null }, ifNull)

/**
 * Return result of executing [ifNotNull] if not null or return null
 */
inline fun <T, R> T?.ifNotNull(ifNotNull: (T) -> R) =
        checkNullable(ifNotNull, { null })

/**
 * Return [this] if not null, or result of executing [ifNull]
 */
inline fun <T> T?.elvis(ifNull: () -> T) = this ?: ifNull()

inline fun <reified T> isNullable() = null is T

fun <T> T?.sureNotNull() = this!!