package org.hnau.base.extensions

/**
 * Return result of [invoke(Unit)]
 */
operator fun <T> ((Unit) -> T).invoke() = this(Unit)

operator fun <A, B, C> ((A) -> B).plus(
        other: (B) -> C
) = { value: A ->
    invoke(value).let(other)
}