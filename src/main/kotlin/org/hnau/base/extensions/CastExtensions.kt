package org.hnau.base.extensions

import org.hnau.base.extensions.boolean.checkBoolean


/**
 * Return `true` if [this] is [R], or `false`
 */
inline fun <reified R> Any?.checkIs() = this is R

/**
 * Return [this] casted to [R]
 */
inline fun <reified R> Any?.cast() = this as R

/**
 * Return result of executing [ifCast] if [this] is [R],
 * or result of executing [ifElse]
 */
inline fun <reified R, O> Any?.castOrElse(
        ifCast: (R) -> O,
        ifElse: () -> O
) = checkIs<R>().checkBoolean(
        ifTrue = { ifCast(this as R) },
        ifFalse = { ifElse() }
)

/**
 * Return [this] if [this] is [R],
 * or result of executing [ifElse]
 */
inline fun <reified R> Any?.castOrElse(ifElse: () -> R) =
        castOrElse<R, R>(ifCast = { it }, ifElse = ifElse)

/**
 * Return [this] if [this] is [R], or `null`
 */
inline fun <reified R> Any?.castOrNull() =
        castOrElse<R?> { null }