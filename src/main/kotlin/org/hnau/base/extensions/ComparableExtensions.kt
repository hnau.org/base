package org.hnau.base.extensions

import org.hnau.base.extensions.boolean.checkBoolean

/**
 * Return result of executing [action] if [this] large than [other], or `null`
 */
inline fun <O, T : Comparable<O>, R> T.ifLargeThan(other: O, action: (T) -> R) = letIf({ it > other }, action)

/**
 * Return result of executing [action] if [this] less than [other], or `null`
 */
inline fun <O, T : Comparable<O>, R> T.ifLessThan(other: O, action: (T) -> R) = letIf({ it < other }, action)

/**
 * Return result of executing [action] if [this] not less than [other], or `null`
 */
inline fun <O, T : Comparable<O>, R> T.ifNotLessThan(other: O, action: (T) -> R) = letIf({ it >= other }, action)

/**
 * Return result of executing [action] if [this] not large than [other], or `null`
 */
inline fun <O, T : Comparable<O>, R> T.ifNotLargeThan(other: O, action: (T) -> R) = letIf({ it <= other }, action)


/**
 * Return [this] if large than [other], or `null`
 */
fun <O, T : Comparable<O>> T.takeIfLargeThan(other: O) = ifLargeThan(other, ::it)

/**
 * Return [this] if less than [other], or `null`
 */
fun <O, T : Comparable<O>> T.takeIfLessThan(other: O) = ifLessThan(other, ::it)

/**
 * Return [this] if not less than [other], or `null`
 */
fun <O, T : Comparable<O>> T.takeIfNotLessThan(other: O) = ifNotLessThan(other, ::it)

/**
 * Return [this] if not large than [other], or `null`
 */
fun <O, T : Comparable<O>> T.takeIfNotLargeThan(other: O) = ifNotLargeThan(other, ::it)


fun <T : Comparable<T>> max(
        first: T,
        second: T
) = (first > second).checkBoolean(
        ifTrue = { second },
        ifFalse = { first }
)

fun <T : Comparable<T>> min(
        first: T,
        second: T
) = (first < second).checkBoolean(
        ifTrue = { first },
        ifFalse = { second }
)